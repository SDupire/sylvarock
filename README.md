# SylvaRock #
## Cartographie des forêts avec une fonction de protection potentielle contre les chutes de blocs/ Mapping of forests with a potential protective effect against rockfalls ##

Version : 2.0  
Language : C (Model), Python (Data formatting)  
Reference : https://hal.archives-ouvertes.fr/hal-02519495v2/document  

Copyright: Sylvain DUPIRE (2018-) *sylvain.dupire@inrae.fr* & David TOE (2018-2020) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ![INRAE](./Documentation/img/Logo-INRAE.jpg?raw=true)
 
&nbsp;  

![Logo_FR](./Documentation/img/FR.png?raw=true) SylvaRock permet de simuler la propagation de blocs rocheux le long de versants à l'échelle régionale dans le but d'identifier et de cartographier les forêts avec une fonction de protection potentielle.  
 
Toutes les forêts situées sur la trajectoire d'un bloc parvenant jusqu'à un enjeu à protéger (Voies ferrées, Bâtiments, Routes...) sont ainsi identifiées.

Le modèle renvoie aussi des informations spatiales sur l'aléa rocheux (Angle de la ligne d'énergie, nombre de blocs arrêtés ou traversant un pixel...)




__Couches nécessaires en entrée :__

| Nom              | Type                | Description                                | Details               |
| :--------------- | :------------------ | :----------------------------------------- | :------------------------------ | 
| Dem.asc          | float32<br />raster | Modèle numérique de terrain                | |
| Forest.asc       | int8<br />raster    | Surfaces forestières                       | 1 : Forêt <br />0 : Hors Forêt  |
| Issues.asc       | int16<br />raster   | Enjeux à protéger                          | 1 : Voies ferrées<br />10 : Bâtiments<br />100 : Routes principales<br />1000 : Routes secondaires<br />10000 : Autres enjeux de moindre importance | 
| Soil_class.asc   | int8<br />raster    | Classes de sol                             | 1 : Zone agricole<br />2 : Forêt<br />3 : Surface minérale<br />4 : Zone urbaine<br />5 : Eau permanente<br />6 : Eau temporaire<br />7 : Eboulis<br />8 : Pelouses et lande alpines | 
| Release_area.asc | int8<br />raster    | Zones de départ de bloc                    | 1 : Zone de départ <br />0 : Autre | 
| Soil_Param.csv   | float32<br />table  | Paramètres associés à chaque classe de sol |  | 

Tous les rasters doivent avoir la même emprise spatiale et la même résolution. Ils doivent aussi être dans le même dossier. La valeur de NODATA de toutes les couches doit être fixée à -9999.   


__Lancement du modèle :__  
Ligne de commande pour exécuter le programme (uniquement sous Linux) :

./main -nthr 1 -path ./ -nsim 500 -vol 5. -inifall 0.5  
&nbsp;  

-nthr : nombre de coeurs (recommandé 1)  
-path : chemin complet vers le dossier contenant les entrées obligatoires  
-nsim : nombre de blocs lancés par cellule de départ  
-vol : volume des blocs lancés en m3  
-inifall : hauteur de chute en m  

Tags = ___forêt___, ___protection___, ___mnt___, ___cartographie___, ___montagne___, ___chutes-de-blocs___   

&nbsp;  

------------------------------------


![Logo_EN](./Documentation/img/UK.png?raw=true) Sylvarock allows to simulate rockfalls propagation along slopes at a regional scale with the objective to identify and map forests with a potential protective effect.  

All forests located on a rockfall trajectory that impacts human issues (Railways, Buildings, Roads) are identified.

The model also returns spatial information on rockfall hazard such as number of enrigie line angle, rocks stopped or passing through a raster cell.

__Compulsory inputs:__

| Name             | Type                | Description                                                                  | Details               |
| :--------------- | :------------------ | :--------------------------------------------------------------------------- | :------------------------------ | 
| Dem.asc          | float32<br />raster | Digital elevation model         | |
| Forest.asc       | int8<br />raster    | Forest area                     | 1 : Forest<br />0 : No Forest |
| Issues.asc       | int16<br />raster   | Human issues                    | 1 : Railways<br />10 : Buildings<br />100 : Main roads<br />1000 : Secondary roads<br />10000 : Other issues of lesser importance | 
| Soil_class.asc   | int8<br />raster    | Soil classes                    | 1 : Agricultural area<br />2 : Forest<br />3 : Mineral area<br />4 : Urban area<br />5 : Permanent water area<br />6 : Temporary water area<br />7 : Screes<br />8 :  Lawn and Alpine heath  | 
| Release_area.asc | int8<br />raster    | Rockfall release areas          | 1 : Release area<br />0 : Other | 
| Soil_Param.csv   | float32<br />table  | Parameters of each soil classes |  | 

All rasters must have the same spatial extent and cell resolution, they must be in the same folder. NODATA value must be -9999.  

__How to launch Sylvarock :__   
Command line to execute Sylvarock (only Linux) :

./main -nthr 1 -path ./ -nsim 500 -vol 5. -inifall 0.5  
&nbsp;   

-nthr : number of threads (we recommend using 1)  
-path : complete path to the folder containing compulsory inputs  
-nsim : number of rocks to propagate for each release area cell  
-vol : volume of rocks in m3  
-inifall : height of initial fall in meter    


Tags = ___forest___, ___protection___, ___dem___, ___mapping___, ___mountain___, ___rockfall___   

