#include "tool_h.h"
#include "global.h"
#include "tool_tens.h"
#include "hash0.h"

struct threadData{
  int length;
  struct Point *index;
  char *messages;
};
struct Image {
    int w, h;
    float **data;
};

int SylvaRock(const char *, int, int,float, float,char **, struct Image*, void (*tick)(int));
//void freeImage(struct Image*);

//QProgressBar *progressBar;

