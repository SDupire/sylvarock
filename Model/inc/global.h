#ifndef _GLOBAL_H_
#define _GLOBAL_H_

//#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
/*------------------------------------------------------------------------------------*/
              /*PARAMETERS*/
/*------------------------------------------------------------------------------------*/
#define VERSION_YEAR 2022
#define FAILURE 0
#define SUCCESS 1
#define INVAL_DATE -5
#define _METHOD 1
#define PHI 3.14159265359
#define EPSILON .000001
#define THRESHOLD 55
#define UPDATE_VALUE 89

/*------------------------------------------------------------------------------------*/
                               /*MACROS*/
/*------------------------------------------------------------------------------------*/
#define NaN NAN
#define INF __builtin_inf()
#define sign(a) ((a >= 0) ? 1 : -1)
#define Max(a,b) ((a > b) ? a : b)
#define Min(a,b) ((a < b) ? a : b)
#define fix(a) ((a > 0) ? floor(a) : ceil(a))
#define sqr(a) ((a) * (a))
#define equ(x, y) fabs(x - y) < EPSILON
/*------------------------------------------------------------------------------------*/
                           /*ASC FILES*/
/*------------------------------------------------------------------------------------*/
#define DEM_FILE "Dem.asc"
#define RALEA_FILE "Release_Area.asc"
#define SOIL_FILE "Soil_class.asc"
#define ISSUES_FILE "Issues.asc"
#define FOREST_FILE "Forest.asc"

/*------------------------------------------------------------------------------------*/
                           /*CSV FILES*/
/*------------------------------------------------------------------------------------*/
#define SOIL_PARAM "Soil_Param.csv"


#endif

