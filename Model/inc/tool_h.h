#ifndef _TOOL_H_
#define _TOOL_H_

#include "tool_tens.h"
/*------------------------------------------------------------------------------------*/
int slopecalc(float **dem, int r, int c, float cellSize, float noDataValue, float **slope_arr);
/*------------------------------------------------------------------------------------*/
int aspectcalc(float **dem, int r, int c, float cellSize, int method, float **aspectarr);
/*------------------------------------------------------------------------------------*/
int convert_soiltype(int **soiltype, int row, int col, float **res);
/*------------------------------------------------------------------------------------*/
int distance_left_in_cell(float richting, float X, float Y, float cellsize, float *maxdist);
/*------------------------------------------------------------------------------------*/
int impact_depth(float R, float Rn, float V, float RockMass, float *dp);
/*------------------------------------------------------------------------------------*/
int direction_after_rebound(float aspect, float velocity, float *richting);
/*------------------------------------------------------------------------------------*/
int flight(float slope, float g, float maxdist, float R, float *Z, float *V_hor, float *V_vert, float *new_distance, float *passHeight, float *t);
/*------------------------------------------------------------------------------------*/
int calc_new_pos (float maxdist, float richting, float cellsize, int start_r, int start_c, float Xcoor_start, float Ycoor_start, float impact,
              float *X, float *Y, int *r, int *c, float *Xcoor, float *Ycoor,float *new_distance );
/*------------------------------------------------------------------------------------*/
int rebound(float slope, float I,int rnin, int indBat, float RockMass, float R, float *V_hor, float *V_vert, float *V_rot, int r, int c,float **SoilParam);
/*------------------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------------------*/


