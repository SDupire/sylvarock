/***************************************************************************
 *cr
 *cr            (C) Copyright 1995-2004 The Board of Trustees of the
 *cr                        University of Illinois
 *cr                         All Rights Reserved
 *cr
 ***************************************************************************/

/***************************************************************************
 * RCS INFORMATION:
 *
 *      $RCSfile: hash.h,v $
 *      $Author: johns $        $Locker:  $             $State: Exp $
 *      $Revision: 1.5 $      $Date: 2004/01/29 23:45:50 $
 *
 ***************************************************************************
 * DESCRIPTION:
 *   A simple hash table implementation for strings, contributed by John Stone,
 *   derived from his ray tracer code.
 ***************************************************************************/
#ifndef __HASH_
#define __HASH_

#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct hash_node {
  double data;                      /* data in hash node */
  char * key;                   /* key for hash lookup */
  char * replace;		/* main categorie for key (ex: bill clinton / president clinton) */
  struct hash_node *next;           /* next node in hash chain */
} hash_node_t;

typedef struct {
  hash_node_t **bucket;        /* array of hash nodes */
  int size;                           /* size of the array */
  int entries;                        /* number of entries in table */
  int downshift;                      /* shift cound, used in hash function */
  int mask;                           /* used to select bits for hashing */
} hash_t;


#define HASH_FAIL -1

void hash_init(hash_t *, int);

double hash_lookup (const hash_t *, const char *);

char *hash_lookup2 (const hash_t *, const char *);

int hash_insert (hash_t *, const char *, double);

int hash_insert2(hash_t *, const char *, const char *, double);

double hash_delete (hash_t *, const char *);

void hash_destroy(hash_t *);

char *hash_stats (hash_t *);

char **getNodes(hash_t, int *);

void inc_data(const hash_t *tptr, const char *key);

void inc_data2(const hash_t *tptr, const char *key, double add);

#ifdef __cplusplus
}
#endif

#endif

