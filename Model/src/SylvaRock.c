  /*-----------------------------------------------------------------------------------------------*/
  /* Main function of Sylvarock. Version adapted in 2018 by David Toe base on RF3D script from 2012*/
  /*                     Remasteresied by Sylvain Dupire in 2022                                   */
  /*      Mainly contain functions to retrieve informations from the command line                  */
  /*-----------------------------------------------------------------------------------------------*/

#include <pthread.h>
#include "SylvaRock.h"

//Related to multi threading
#define _MULTI_THREADED
pthread_mutex_t  mutex  = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t  mutex_vec = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t  mutex_rc = PTHREAD_MUTEX_INITIALIZER;

//Used for ResultPy creation.
FILE *pFile;
void (* myTick)(int) = 0x0;
HEADER head;
FILE *binFile_data_calc;
//hash_t;
/*------------------------------------------------------------------------------------*/
//Initialisation of the variables
struct DataCalcscr *Data_calcscr = 0x0;
int total_nr_calcs=0, calc_nr, nr_Data_calcscr = 0;
int nr_simulations,with_nets,id_simu;
float ini_fallheight;
float INI_Volume;
float **dem, **SoilParam;

int  **countingBlockPass, **countingBlockArrested,**ReAlea,**Forest, **Issues;
int **TabForest, **Soil, **TabIssues,**Dpar,**NbRel,**NbRelTemp, nbRocks;
float **ELgrid, **ELMeasured, **slopearr, **aspectarr;
char *Path;
//int CountSim=0;

struct stat FolderExist = {0};
//float **ELgridMin,**ArretCond,**ELPred

#define checkResults(string, val) {             \
 if (val) {                                     \
   printf("Failed with %d at %s", val, string); \
   exit(1);                                     \
 }                                              \
}

//Funtion created based on Sylvain DUPIRE script, used to upload pixel value of the 4_PassingImpactingIsssues raster indicating if the block have touch a road, a building, a house or a combination of these three elements
int code_enjeu(int prev_enjeu, int new_enjeu){
	int factor100000 = 0;
	int factor10000 = 0;
	int factor1000 = 0;
	int factor100 = 0;
	int factor10 = 0;
	int factor1 = 0;
	int factor100000_2 = 0;
	int factor10000_2 = 0;
	int factor1000_2 = 0;
	int factor100_2 = 0;
	int factor10_2 = 0;
	int factor1_2 = 0;
	int resInd = 0;

	if ((prev_enjeu-100000)>=0)														 	                	{factor100000=1;}
	if ((prev_enjeu-10000-factor100000*100000)>=0)															{factor10000=1;}
	if ((prev_enjeu-1000-factor100000*100000-factor10000*10000)>=0) 										{factor1000=1;}
	if ((prev_enjeu-100-factor100000*100000-factor10000*10000-factor1000*1000)>=0) 							{factor100 = 1;}
	if ((prev_enjeu-10-factor100000*100000-factor10000*10000-factor1000*1000-factor100*100)>=0) 			{factor10 = 1;}
	if ((prev_enjeu-1-factor100000*100000-factor10000*10000-factor1000*1000-factor100*100-factor10*10)>=0)	{factor1 = 1;}

	if ((new_enjeu-100000)>=0)  																					{factor100000_2=1;}
	if ((new_enjeu-10000-factor100000_2*100000)>=0)  																{factor10000_2=1;}
	if ((new_enjeu-1000-factor100000_2*100000-factor10000_2*10000)>=0)												{factor1000_2=1;}
	if ((new_enjeu-100-factor100000_2*100000-factor10000_2*10000-factor1000_2*1000)>=0)								{factor100_2 = 1;}
	if ((new_enjeu-10-factor100000_2*100000-factor10000_2*10000-factor1000_2*1000-factor100_2*100)>=0)				{factor10_2 = 1;}
	if ((new_enjeu-1-factor100000_2*100000-factor10000_2*10000-factor1000_2*1000-factor100_2*100-factor10_2*10)>=0)	{factor1_2 = 1;}

	resInd += 100000*Max(factor100000,factor100000_2);
	resInd += 10000*Max(factor10000,factor10000_2);
	resInd += 1000*Max(factor1000,factor1000_2);
	resInd += 100*Max(factor100,factor100_2);
	resInd += 10*Max(factor10,factor10_2);
	resInd += 1*Max(factor1,factor1_2);

    return (resInd);
}
///////////////////////////////////////////////////////////////////////
//Function to load the input raster file
///////////////////////////////////////////////////////////////////////
int loadFiles_thr(const char *path, HEADER head, int file_ind, char **message){
	HEADER head1;
	*message=strdup("");
	switch(file_ind) {
		//case (1): {
        //IdCell
        //if (hdrload_f ( path, ID_CELL, &head1, 0.0, &IdCell) == FAILURE) { *message=strcat3("Error: File ",ID_CELL," can not be loaded\n"); return(FAILURE); }
        //if(head1.cols != head.cols || head1.rows != head.rows || head1.x != head.x || head1.y != head.y)  { *message =strcat2("Error: input rasters don't have the same map extent or resolution: please check file ",ID_CELL); return(FAILURE); }
        //break;
      	//}
      	case (2): {//ReAlea
        	if (hdrload_i ( path, RALEA_FILE, &head1, 0.0, &ReAlea) == FAILURE) { *message=strcat3("Error: File ",RALEA_FILE," can not be loaded\n"); return(FAILURE); }
        	if(head1.cols != head.cols || head1.rows != head.rows || head1.x != head.x || head1.y != head.y)  { *message =strcat2("Error: input rasters don't have the same map extent or resolution: please check file ",RALEA_FILE); return(FAILURE); }
        	break;
      	}
      	case (9): {//Soil
      		if (hdrload_i ( path, SOIL_FILE, &head1,0.0, &Soil) == FAILURE) { *message=strcat3("Error: File ",SOIL_FILE," can not be loaded\n"); return(FAILURE); }
      		if(head1.cols != head.cols || head1.rows != head.rows || head1.x != head.x || head1.y != head.y) {*message =strcat2("Error: input rasters don't have the same map extent or resolution: please check file ",SOIL_FILE);return(FAILURE);}
      		break;
      	}
      	case (10): {//Forest
       		if (hdrload_i ( path, FOREST_FILE, &head1,0, &Forest) == FAILURE) { *message=strcat3("Error: File ",FOREST_FILE," can not be loaded\n"); return(FAILURE); }
       		if(head1.cols != head.cols || head1.rows != head.rows || head1.x != head.x || head1.y != head.y) { *message =strcat2("Error: input rasters don't have the same map extent or resolution: please check file ",FOREST_FILE); return(FAILURE);}
       		break;
      	}
      	case (11): {//Issues
      		if (hdrload_i ( path, ISSUES_FILE, &head1, NaN, &Issues) == FAILURE) { *message=strcat3("Error: File ",ISSUES_FILE," can not be loaded\n"); return(FAILURE); }
			if(head1.cols != head.cols || head1.rows != head.rows || head1.x != head.x || head1.y != head.y) {*message =strcat2("Error: input rasters don't have the same map extent or resolution: please check file ",ISSUES_FILE);return(FAILURE);}
			break;
		}
	}
	return (SUCCESS);
}

//Function related to multithreading
struct threadDataLoad{
    int nbFiles;
    int error;
    int *file_indx;
    char *message;
};

void *doThread_Load(void *parm){
	struct threadDataLoad *data = (struct threadDataLoad*) parm;
	int i;
	for(i=0; i < data->nbFiles; i++){
		if (loadFiles_thr(Path, head, data->file_indx[i], &(data->message))==FAILURE){
			printf("%s\n",data->message); fflush(stdout);
			data->error = 1;
        	return (void *)1; }
	}
 	free1a(data->file_indx, data->nbFiles, sizeof(int));
	return (void *)1;
}
/*-------------------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------------------------*/
// Create grid filled whith zeros whith the same number of row and colomn as the raster loaded
int init_all_results( char **textbox_messages) {
	if (!(countingBlockPass =  zero_i(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix countingBlockPass.."); return(FAILURE); }
	//if (!(ArretCond =  zero_i(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix ArretCond.."); return(FAILURE); }
	if (!(countingBlockArrested =  zero_i(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix countingBlockArrested.."); return(FAILURE); }
	if (!(Dpar =  zero_i(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix Dpar.."); return(FAILURE); }
	if (!(NbRel =  zero_i(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix NbRel.."); return(FAILURE); }
	if (!(NbRelTemp =  zero_i(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix NbRelTemp.."); return(FAILURE); }
	if (!(ELgrid = zero(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix ELgrid.."); return(FAILURE); }
	//if (!(ELgridMin = zero(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix ELgridMin.."); return(FAILURE); }
	if (!(TabForest = zero_i(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix TabForest.."); return(FAILURE); }
	if (!(TabIssues = zero_i(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix TabIssues.."); return(FAILURE); }
	//if (!(ELPred = zero(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix ELPred.."); return(FAILURE); }
	if (!(ELMeasured = zero(head.rows, head.cols))) { *textbox_messages=strdup("Can not allocate matrix ELMeasured.."); return(FAILURE); }
	return(SUCCESS);
}
/*-------------------------------------------------------------------------------------------------------------------------------------*/
// Not sure shoud be used to clear the memory somewhere
void free_all() {
	if (dem) free2a(dem, head.rows, head.cols, sizeof(float));
	if (slopearr) free2a(slopearr, head.rows, head.cols, sizeof(float));
	if (aspectarr) free2a(aspectarr, head.rows, head.cols, sizeof(float));
	//if (rn) free2a(rn, head.rows, head.cols, sizeof(float));
	//if (IdCell) free2a(IdCell, head.rows, head.cols, sizeof(float));
	if (ReAlea) free2a(ReAlea, head.rows, head.cols, sizeof(float));
	if (Issues) free2a(Issues, head.rows, head.cols, sizeof(float));
	//if (ArretCond) free2a(ArretCond, head.rows, head.cols, sizeof(int));
	if (countingBlockPass) free2a(countingBlockPass, head.rows, head.cols, sizeof(int));
	if (Dpar) free2a(Dpar, head.rows, head.cols, sizeof(int));
	if (NbRel) free2a(NbRel, head.rows, head.cols, sizeof(int));
	if (NbRelTemp) free2a(NbRelTemp, head.rows, head.cols, sizeof(int));
	if (countingBlockArrested) free2a(countingBlockArrested, head.rows, head.cols, sizeof(int));
	if (ELgrid) free2a(ELgrid, head.rows, head.cols, sizeof(float));
	//if (ELgridMin) free2a(ELgridMin, head.rows, head.cols, sizeof(float));
	if (TabForest) free2a(TabForest, head.rows, head.cols, sizeof(float));
	if (TabIssues) free2a(TabIssues, head.rows, head.cols, sizeof(float));
	if (ELMeasured) free2a(ELMeasured, head.rows, head.cols, sizeof(float));
	//if (ELPred) free2a(ELPred, head.rows, head.cols, sizeof(float));
	if (Forest) free2a(Forest, head.rows, head.cols, sizeof(float));
}
/*-------------------------------------------------------------------------------------------------------------------------------------*/
// Main function where the block propagate loop on the release area location i = row, j = column
int doALL(int i, int j, char **textbox_messages){
	int k, rc, flag, rebound_nr, r, c, rin, cin, start_r, start_c;
	int impact, check, rnin;
	float aspect, slope;
	float maxdist, new_distance, old_distance, ddist_entre_rebonds, V_before_impact;
	float *vec, X, g, Y, Z, V_hor, V_vert, Vrot, velocity, PassHeight, t;
	float RockMass, I, R, RockVolume, traj_time, Xcoor_start, Ycoor_start, Xcoor, Ycoor, richting;
	char token[100];
	hash_t dummygrd_hash;
	hash_init(&dummygrd_hash,128);// head.cols*head.rows);
	if(!(vec = (float*)malloc1a(3,sizeof(int)))) return (FAILURE);
//******************************************************************************************//
//New variable defined by David Toe to implement the energy line stopping criteria
	int cellEnCour;
	float denivele,EnergyLineMeas,CumFor;
	float DistParc;
	//int CountSim;
	int flagVerif=0;
	int indBat=0;
	int flagIssue;
	int CumFor2;
//******************************************************************************************//
//******************************************************************************************//
    //New variable to have a differenrial numlber of rocks release by release area
	int nr_sim_cell=nr_simulations*ReAlea[i][j];
	//New variable to have the number of release area for each pixel
	NbRel[i][j] = NbRel[i][j]+ReAlea[i][j];
	NbRelTemp[i][j] = id_simu;

//******************************************************************************************//
//******************************************************************************************//

	//CountSim=0;
// Do the n simulation per source cell
	for(k=0; k< nr_sim_cell; k++) {
	// Local variable definition
		hash_t dummygrd2_hash;
		hash_init(&dummygrd2_hash,32);
		calc_nr++;
		X = 0;
		Y = 0;
		Z = ini_fallheight;
		V_hor = 0.5; V_vert = -0.5; Vrot = 0.1; g = -9.81;
		start_r = i;
		start_c = j;
		//CountSim=CountSim+1;
		int rFollow[10000];
		int cFollow[10000];
		CumFor=0;
		cellEnCour=0;
		rFollow[cellEnCour]=start_r;
		cFollow[cellEnCour]=start_c;
		flagIssue=0;
//******************************************************************************************//
		// Change made by David Toe. The volume of the bloc is directly define by the user in command line
		//RockVolume =INI_Volume;
		RockVolume =unifRand()*(4.9) +0.1;
		RockMass = 2700. * (RockVolume);
		I = (RockMass) * (pow(RockVolume, 1.0/3.0)*pow(RockVolume, 1.0/3.0) + pow(RockVolume, 1.0/3.0)*pow(RockVolume, 1.0/3.0)) /12.0;
		R = (pow(RockVolume, 1.0/3.0)) /2.0;
//******************************************************************************************//
		traj_time=0;
		Xcoor_start = head.x + j * head.cellSize;
		Ycoor_start = head.y + (head.rows -i-1) * head.cellSize;
		Xcoor = Xcoor_start;
		Ycoor = Ycoor_start;
		richting = aspectarr[i][j];
		velocity = sqrt(V_hor *V_hor + V_vert *V_vert);
		flag = 1;
		rebound_nr = 0;
		r=i;
		c=j;
		check =0;
		DistParc=0;
//******************************************************************************************//
		// Function running until the current simulated block is stopped. Here we can define (if we want) the new condition for block stopping.
		while ((velocity>0.2) && (r < head.rows-2) && (c < head.cols-2) && (r > 1) & (c > 1) &&
             (dem[r][c]>=0) && (flag==1) && isnan(richting)==0 && isnan(aspectarr[r][c])==0 &&
			 (Soil[r][c]>0))
			{
			check++;
			rin = r;
			cin = c;
			aspect = aspectarr[r][c];
			slope = (asin((sin(slopearr[r][c]*PHI/180.0)*cos((aspect-richting)*PHI/180.0))))*180.0/PHI;
			//using issues cover to modified Rn and Rg for road and building
			indBat=Issues[r][c];
			 //search the soiltype
			rnin = Soil[r][c];
//******************************************************************************************////******************************************************************************************//
			//RF3D basic propagation code
			impact = 0;
			new_distance = 0;
			if(distance_left_in_cell(richting, X, Y, head.cellSize, &maxdist)== FAILURE){*textbox_messages=strdup("ERROR: in distance_left_in_cell!!!\n");return(FAILURE);}
			old_distance = new_distance;
			if(flight(slope, g, maxdist, R, &Z, &V_hor, &V_vert, &new_distance, &PassHeight, &t)== FAILURE) {*textbox_messages=strdup("ERROR: in flight!!!\n");return(FAILURE);}
			traj_time += t;
//******************************************************************************************************************//
//******************************************************************************************************************//
			//RF3D basic propagation code
			ddist_entre_rebonds = new_distance - old_distance;
			if ( (impact == 0) && (ddist_entre_rebonds < Min(R,0.2)) && (Z == 0) && (rebound_nr > 1) &&
			(sqrt(V_hor*V_hor + V_vert*V_vert) < 5.0) && (slope >= 0) && (slope*180.0/PHI < 30)) {
			new_distance = Min((old_distance + Min(R,0.2)), maxdist);}

			velocity = flag *sqrt(V_hor *V_hor + V_vert *V_vert);
			if(calc_new_pos(maxdist, richting, head.cellSize, start_r, start_c,Xcoor_start, Ycoor_start, (float)impact, &X, &Y, &r, &c, &Xcoor, &Ycoor, &new_distance)== FAILURE){*textbox_messages=strdup("ERROR: in calc_new_pos!!!\n");return(FAILURE);}
			V_before_impact = sqrt(V_hor *V_hor + V_vert *V_vert);

			if (fix(Z*1000)/1000.0 ==0) {  //Rock hits the slope surface
				if(rebound(slope, I, rnin,indBat, RockMass, R, &V_hor, &V_vert, &Vrot, r, c, SoilParam) == FAILURE) {*textbox_messages=strdup("ERROR: in rebound!!!\n");return(FAILURE); }
				rebound_nr++;
				if(direction_after_rebound(aspect, V_before_impact, &richting) == FAILURE) {*textbox_messages=strdup("ERROR: in direction_after_rebound!!!\n");return(FAILURE);}
				if(distance_left_in_cell(richting, X, Y, head.cellSize, &maxdist) == FAILURE) {*textbox_messages=strdup("ERROR: in distance_left_in_cell!!!\n");return(FAILURE);}
				new_distance = 0;
			}

			velocity = flag *sqrt(V_hor *V_hor + V_vert *V_vert);
			if ((r != rin || c != cin ) && flag == 1 ) {
				rc = pthread_mutex_lock(&mutex);
				checkResults("pthread_mutex_lock()\n", rc);
				sprintf(token,"%d_%d",rin,cin);
				if(hash_lookup(&dummygrd2_hash, token) < 0){
					hash_insert(&dummygrd2_hash, token, 100);
					countingBlockPass[rin][cin]++;
				}
				check = 0;
				rc = pthread_mutex_unlock(&mutex);
				checkResults("pthread_mutex_unlock()\n", rc);
			}
			else if (check > (head.cellSize/(R/50.0)))  flag = 0;
//*****************************************************************************************************************************//
//*****************************************************************************************************************************//
//*****************************************************************************************************************************//
			//Modifications integrate by David Toe. Use to identify the forest with a protection function and calculate normalized and complementary area to use stats from Distriblock
			denivele=abs(dem[start_r][start_c]-dem[r][c]);
			EnergyLineMeas=atan(denivele/sqrt(pow((start_r-r)*head.cellSize,2.)+pow((start_c-c)*head.cellSize,2.)))*180./3.14159265359;//////////FiXME////////Recalculate the relation from the last data base
			ELMeasured[r][c] = Max(EnergyLineMeas, ELMeasured[r][c]);
			//if(ELgridMin[r][c]==0){ELgridMin[r][c]=EnergyLineMeas;}
			//if(ELgridMin[r][c]!=0){ELgridMin[r][c] = Min(EnergyLineMeas, ELgridMin[r][c]);}

			//Calculation of the minimum distance from release
			DistParc=Max(DistParc+ddist_entre_rebonds,0);
			if (Dpar[r][c]==0) {
				Dpar[r][c]=(int)(DistParc+0.5);}
			else {
				Dpar[r][c]=Min((int)(DistParc+0.5),Dpar[r][c]);}
			//Calculation of nb of release area
			if (NbRelTemp[r][c]<id_simu) {
				NbRelTemp[r][c]=id_simu;
				NbRel[r][c] = NbRel[r][c]+ReAlea[i][j];}
			/////Calcul des distance entre deux rebonds
			cellEnCour=cellEnCour+1;
			rFollow[cellEnCour]=r;
			cFollow[cellEnCour]=c;

			//Calculation of the distance a block have travel through a forest
			if(Forest[r][c]!=0){CumFor=Max(CumFor+ddist_entre_rebonds,0);}

//     if ((CumFor>5000)){
// printf("----------------------------------------\n");
// printf("%f \n", CumFor);fflush(stdout);
// printf("%f \n", DistRebAvAp);fflush(stdout);
// printf("%f \n", ddist_entre_rebonds);fflush(stdout);
// printf("%f \n", DistParc);fflush(stdout);
// printf("%f \n", DistParc2);fflush(stdout);
// printf("%f \n", velocity);fflush(stdout);
// printf("%f \n", RockVolume);fflush(stdout);
// printf("----------------------------------------\n");

//     }

			if (Issues[r][c]>0){
			flagIssue=1;
			CumFor2=(int)(CumFor+0.5);
			int iIn;
			for(iIn=0;iIn<cellEnCour;iIn++){
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Option to map only the forest having a protection function (Here we have pixel forest having a value of travelling distance in forest)
				if (Forest[rFollow[iIn]][cFollow[iIn]]>0){
					if(TabForest[rFollow[iIn]][cFollow[iIn]]==0){
						TabForest[rFollow[iIn]][cFollow[iIn]]=CumFor2;
					}
					if(TabForest[rFollow[iIn]][cFollow[iIn]]!=0){
						TabForest[rFollow[iIn]][cFollow[iIn]]=Min(TabForest[rFollow[iIn]][cFollow[iIn]],CumFor2);
					}
				}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Option to map all the block trajectory (Here we have pixel having a value of travelling distance in forest)
// if(TabForest[rFollow[iIn]][cFollow[iIn]]==0){TabForest[rFollow[iIn]][cFollow[iIn]]=CumFor;}
// if(TabForest[rFollow[iIn]][cFollow[iIn]]!=0){Min(TabForest[rFollow[iIn]][cFollow[iIn]],CumFor);}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//*****************************************************************************************************************************//
				TabIssues[rFollow[iIn]][cFollow[iIn]]=code_enjeu(TabIssues[rFollow[iIn]][cFollow[iIn]],Issues[r][c]);
			}
			}
//*****************************************************************************************************************************//
//*****************************************************************************************************************************//
//*****************************************************************************************************************************//
    //RF3D basic function
		} //rocks goes into next raster cell
		rc = pthread_mutex_lock(&mutex_rc);
		checkResults("pthread_mutex_lock()\n", rc);
		{
		sprintf(token,"%d_%d",r,c);
		if(hash_lookup(&dummygrd2_hash, token) < 0){hash_insert(&dummygrd2_hash, token, 100);countingBlockPass[r][c]++; }
		}
		rc = pthread_mutex_unlock(&mutex_rc);
		checkResults("pthread_mutex_unlock()\n", rc);
		hash_destroy(&dummygrd2_hash);
		countingBlockArrested[r][c]++;

		if ((flagVerif==1)) {
			rc = pthread_mutex_lock(&mutex_vec);
			checkResults("pthread_mutex_lock()\n", rc);
			if ((r==start_r)&&(c==start_c)){vec[0] = (int)1;
			} else {vec[0] = (int)0;}
		    vec[1] = (int)r;
		    vec[2] = (int)c;
			fprintf(pFile,"%g ",vec[0]);
			fprintf(pFile,"%g ",vec[1]);
			fprintf(pFile,"%g",vec[2]);
			fprintf(pFile,"\n");
		    //fwrite(vec, 5 , sizeof(int) , binFile_data_calc);//At the beginning this vector was created for 9 variable. Never changed since that time.
		    rc = pthread_mutex_unlock(&mutex_vec);
		    checkResults("pthread_mutex_unlock()\n", rc);
		}



	} //Finished calculating one rock trajectory
//*****************************************************************************************************************************//
//*****************************************************************************************************************************//
//*****************************************************************************************************************************//
	hash_destroy(&dummygrd_hash);
	free1a(vec,3,sizeof(int));
	return(SUCCESS);
}

//Function related to multi threading
void *doThread(void *parm){
	struct threadData *data = (struct threadData*) parm;
	int i;
	for(i=0; i < data->length; i++){
		if(doALL(data->index[i].x, data->index[i].y, &(data->messages))==FAILURE) {printf("ERROR\n"); fflush(stdout);};
		nr_Data_calcscr++;
		id_simu++;
		if (myTick) myTick((int) (nr_Data_calcscr * 100 / (float) nbRocks));
	}
	return(NULL);
}
/*-------------------------------------------------------------------------------------------------------------------------------------*/

//Head function gathering information from the command line and launching the blocks. Results files are also initialized here.
int SylvaRock(const char *path, int nbThreads, int nr_sim,float ini_volume, float ini_fall,char **textbox_messages, struct Image* image, void (*tick)(int)){
	myTick = tick;
	int i, j;
	char *pathFile;
	char binFile[]="data_calcscr_res.bin";
	int nbThreads_f= 2*nbThreads, k, nbFiles=13;
	pthread_t  threads[nbThreads];
	int rc=0, xx, yy, ind;
	struct threadData data[nbThreads];
	nr_simulations=nr_sim;
	INI_Volume=ini_volume;
	ini_fallheight=ini_fall;
	////////////////////////////////////
	//pFile = fopen ("trajectory","w+");
	////////////////////////////////////
	id_simu=1;
	nr_Data_calcscr=0;
	time_t start_clock;
	srand(time(&start_clock));
	*textbox_messages="";
	pthread_t threadsLoad[nbThreads_f];
	struct threadDataLoad dataLoad[nbThreads_f];
	myTick(-1);


	//load file param
	if (load_soilParam (path, SOIL_PARAM, -9999,&SoilParam) == FAILURE) { *textbox_messages=strcat3("Error: File ",SOIL_PARAM," can not be loaded\n"); return(FAILURE); }

	//for (i=0;i<8;i++) printf("%f,%f,%f,%f,%f\n",SoilParam[i][0],SoilParam[i][1],SoilParam[i][2],SoilParam[i][3],SoilParam[i][4]);


	// Loading the raster files
	if (hdrload_f ( path, DEM_FILE, &head, NaN, &dem) == FAILURE) { *textbox_messages=strcat3("Error: File ",DEM_FILE," can not be loaded\n"); return(FAILURE); }
	Path=strdup(path);
	if (nbFiles < nbThreads_f) nbThreads_f =nbFiles;
	yy = nbFiles % nbThreads_f;
	k = 1;
	for(i = 0; i < nbThreads_f; i++) {
		dataLoad[i].nbFiles = nbFiles /nbThreads_f;
		if (yy) { yy--;dataLoad[i].nbFiles++;}
		if (!(dataLoad[i].file_indx = (int*) malloc1a(dataLoad[i].nbFiles, sizeof(int))))  return(FAILURE);
		for(j = 0; j < dataLoad[i].nbFiles; j++)  if(k <= nbFiles) dataLoad[i].file_indx[j]=k++;
		dataLoad[i].error = 0;
	}

	for(i = 0; i < nbThreads_f; i++){rc = pthread_create(&threadsLoad[i], NULL, doThread_Load, (void *) &dataLoad[i]);if (rc){printf("ERROR; return code from pthread_create() is %d\n", rc); exit(-1);}  }
	for (i = 0; i < nbThreads_f; i++) {rc = pthread_join(threadsLoad[i], NULL);*textbox_messages=strcat2(*textbox_messages,dataLoad[i].message); if (rc) exit(-1); }
	free(Path);
	for (i = 0; i < nbThreads_f; i++) if(dataLoad[i].error){ *textbox_messages = strdup(dataLoad[i].message); return(FAILURE); }

	//Creating Rn layer. Related to converting soil type. Not used here. See directly in tools.h
	//if (!(rn =(float**) malloc2a(head.rows, head.cols, sizeof(float)))) { *textbox_messages=strdup("Can not allocate matrix rn!!!");return(FAILURE); }
	//if (convert_soiltype(Soil, head.rows, head.cols, rn) == FAILURE) { return(FAILURE); }
	//****************************************************************************************************************************//
	// Calculation of slope and aspect maps
	if (! (slopearr = (float **) malloc2a( head.rows, head.cols, sizeof(float))))  return(FAILURE);
	if (! (aspectarr = (float **) malloc2a( head.rows, head.cols, sizeof(float))))  return(FAILURE);
	if (slopecalc(dem, head.rows, head.cols, head.cellSize, head.noDataValue, slopearr)== FAILURE) return(FAILURE);
	if (aspectcalc(dem, head.rows, head.cols, head.cellSize, _METHOD, aspectarr)== FAILURE) return(FAILURE);
	//****************************************************************************************************************************//
	if(init_all_results(textbox_messages) == FAILURE) return(FAILURE);
	for(i =0; i <head.rows; i++) for(j =0; j <head.cols; j++)
	calc_nr = 0;
	myTick(-2);
	pathFile=strcat2(path, binFile);
	if (fCheckOpen(pathFile, "w", &binFile_data_calc) == FAILURE)  { free (pathFile); return(FAILURE);}
	free(pathFile);

	for(nbRocks =0, i = 0; i < head.rows; i++) for(j=0; j < head.cols; j++)  if (ReAlea[i][j] >0) nbRocks++;
	if(!nbRocks) {free_all();*textbox_messages = strdup("No startcells defined !!!\n");return(FAILURE);}
	total_nr_calcs = nbRocks * nr_simulations;
	if (nbRocks < nbThreads) nbThreads = nbRocks;
	yy = nbRocks %nbThreads;
	for(i = 0; i < nbThreads; i++) {
		data[i].length = nbRocks /nbThreads;
		if (yy) { yy--;data[i].length ++;}
		data[i].messages=strdup("");
	if (!(data[i].index = (struct Point*) malloc1a(data[i].length, sizeof(struct Point))))  return(FAILURE);
	}

	for(xx = 0, yy = 0, ind =0,  i = 0; i < head.rows; i++) for(j=0; j < head.cols; j++)  if (ReAlea[i][j] >0) {
		if (yy >= data[xx].length) {xx++; yy=0;ind = 0;}
		data[xx].index[ind].x = i;
		data[xx].index[ind++].y = j;
		yy++;
	}

	for(i = 0; i < nbThreads; i++){
		rc = pthread_create(&threads[i], NULL, doThread, (void *) &data[i]);
		printf("Thread number %d succesfuly create\n", i);
		if (rc){ printf("ERROR; return code from pthread_create() is %d\n", rc);exit(-1);}
	}

	for (i = 0; i < nbThreads; i++) {
		rc = pthread_join(threads[i], NULL);
		if(data[i].messages) {*textbox_messages=strcat2(*textbox_messages,data[i].messages);free(data[i].messages);}
		if (rc)  exit(-1);
	}

	for(i = 0; i < nbThreads; i++)  free1a (data[i].index, data[i].length, sizeof(struct Point));
	fclose(binFile_data_calc);
//******************************************************************************************************************************************//
	myTick(-3);
	int m = head.rows, n = head.cols;
	float NODATA_value = head.noDataValue;
//******************************************************************************************************************************************//
//******************************************************************************************************************************************//
//Initializing counter used in the main function
	for (i = 0; i < m; i++) for (j = 0; j < n; j++) {
		if (isnan(dem[i][j]) || dem[i][j] == NODATA_value ) {
			countingBlockArrested[i][j] = NODATA_value;
			countingBlockPass[i][j] = NODATA_value;
			Dpar[i][j] = NODATA_value;
			NbRel[i][j] = NODATA_value;
			//******************//
			//ELPred[i][j] = NODATA_value;
			// ELgridMin[i][j] = NODATA_value;
			ELgrid[i][j] = NODATA_value;
			//ArretCond[i][j] = NODATA_value;
			ELMeasured[i][j] = NODATA_value;
		}
	}
//******************************************************************************************************************************************//
//******************************************************************************************************************************************//
//Saving the information from the simulation
// Change 0 value to nodata
	for (i = 0; i < m; i++) for (j = 0; j < n; j++) {
		if (countingBlockArrested[i][j] == 0){countingBlockArrested[i][j] = NODATA_value;}
		if (countingBlockPass[i][j] == 0){countingBlockPass[i][j] = NODATA_value;}
		if (Dpar[i][j] == 0){Dpar[i][j] = NODATA_value;}
		if (NbRel[i][j] == 0){NbRel[i][j] = NODATA_value;}
		if (TabForest[i][j] == 0){TabForest[i][j] = NODATA_value;}
		if (TabIssues[i][j] == 0){TabIssues[i][j] = NODATA_value;}
		if (ELMeasured[i][j] == 0){ELMeasured[i][j] = NODATA_value;}
}

	if (stat(strcat2(path,"/Results"), &FolderExist) == -1) {
		mkdir(strcat2(path,"/Results"), 0700);
	}
	save_asciigrd_i(path, "/Results/1_BlockArrested.asc", &head, countingBlockArrested);
	save_asciigrd_i(path, "/Results/2_BlockPassing.asc", &head, countingBlockPass);
	save_asciigrd_i( path, "/Results/3_ProtectiveForest.asc", &head, TabForest);
	save_asciigrd_i( path, "/Results/4_PassingImpactingIssues.asc", &head, TabIssues);
	save_asciigrd( path, "/Results/5_ELMeasured.asc", &head, ELMeasured);
	save_asciigrd_i( path, "/Results/6_MinReleaseDistance.asc", &head, Dpar);
	save_asciigrd_i( path, "/Results/7_NbReleaseAreas.asc", &head, NbRel);
	remove(strcat2(path,"data_calcscr_res.bin"));
	//save_asciigrd( path, "EL95Pred.asc", &head, ELPred);
	//save_asciigrd( path, "6_ELgridMin.asc", &head, ELgridMin);
	//save_asciigrd_i(path, "ArretAmelie.asc", &head, ArretCond);
	//******************************************************************************************************************************************//
	return (SUCCESS);
}
