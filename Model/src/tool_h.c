#include "tool_h.h"
/*------------------------------------------------------------------------------------*/
int flight(float slope, float g, float maxdist, float R, float *Z, float *V_hor, float *V_vert, float *new_distance, float *passHeight, float *t)
{
  float Zin, V_vertin, V_horin, V, tvn0, passHin, slope_rad, tout1, tout2, tmp;
  Zin = *Z;
  V_vertin = *V_vert;
  V_horin = *V_hor;
  slope_rad = slope* PHI /180.0;
  tmp = (*V_vert) + (*V_hor)*(float)tan(slope_rad);
  tout1 = (-1* tmp - sqrt(tmp*tmp - 2*g*(*Z)))/g;
  tout2 = (-1* tmp + sqrt(tmp*tmp - 2*g*(*Z)))/g;

  *t = Max(tout1, tout2);
  *new_distance= (*V_hor) * (*t);
  *t = *t - Max(0, (*new_distance - maxdist)/(*V_hor));
  *Z = round((*Z + (*V_vert + *V_hor*tan(slope_rad))*(*t) + 0.5*g*sqr(*t))*10000)/10000.0;
  *V_vert = (*V_vert)+g*(*t);
  *new_distance = round((*new_distance) *100) /100.0;
  *new_distance = Min(maxdist, *new_distance);

  tvn0 = ((((*V_hor)*sin(-1*slope_rad))/cos(-1 *slope_rad))-(*V_vert))/g;
  passHin = *Z;

  if (tvn0 < 0) { passHin = Max(*Z, Zin); }
  else if (tvn0 > 0 && tvn0 < *t) {  passHin =  Max(*Z, Zin + 0.5 *g *tvn0 *tvn0 + (V_vertin + V_horin *tan(slope_rad))*tvn0);}

  V = sqrt((*V_hor) *(*V_hor) + (*V_vert) *(*V_vert));
  *V_hor = (*V_hor)/V * Min(V, 41.6667);
  *V_vert = (*V_vert)/V * Min(V, 69.4444);
  *passHeight = Max(R, R+passHin);

 return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
int distance_left_in_cell(float richting, float X, float Y, float cellsize, float *maxdist)
{
  int i;
  float falldirection , *v, *c, t=INF, min;
  falldirection = richting * PHI/180.0;
  if (!(v = (float *) malloc1a(2, sizeof(float)))) return(FAILURE);
  if (!(c = (float *) malloc1a(2, sizeof(float)))) return(FAILURE);
  v[0]= round(cos(falldirection)*100)/100.0;
  v[1]= round(sin(falldirection)*100)/100.0;
  c[0]= X+cellsize/2.0;
  c[1]= Y+cellsize/2.0;
  for(min = INF, i=0; i < 2; i++){
    if(v[i]>0) t = (cellsize-c[i])/v[i];
    else if(v[i]<0) t = -c[i] /v[i];
    t=fabs(t);
    if(t< min) min = t;
  }
  *maxdist = round(min*100)/100.0;
  free1a(v,2, sizeof(float));
  free1a(c,2, sizeof(float));

  return(SUCCESS);
}

/*------------------------------------------------------------------------------------*/
float aspectarr_update(float res, float H, float G)
{
  if ((H > 0) && (G < 0)) { return (res * (180.0 / PHI) + 360); }
  if ((H > 0) && (G > 0)) { return (res * (180.0 / PHI) + 180); }
  if ((H < 0) && (G < 0)) { return (res * 180.0 / PHI); }
  if ((H < 0) && (G > 0)) { return (res * (180.0 / PHI) + 180); }
  if ((H < 0) && (G ==0)) { return (90.0); }
  if ((H > 0) && (G ==0)) { return (270.0); }
  if ((H ==0) && (G < 0)) { return (0.0); }
  if ((H ==0) && (G > 0)) { return (180.0); }
  if ((H ==0) && (G ==0)) { return (-1.0); }
  return (res);
}
/*------------------------------------------------------------------------------------*/
int aspectcalc(float **dem, int r, int c, float cellSize, int method, float **aspectarr)
{
  int i, j;
  float H, G, cellS;
  if (!cellSize ) return(FAILURE);
  //Init the first line and first colmun with zeros...
  for (i = 0; i < r; i++)  aspectarr[i][0] = aspectarr[i][c-1] = 0.0;
  for (j = 0; j < c; j++)  aspectarr[0][j] = aspectarr[r-1][j] = 0.0;
  cellS = (float) cellSize;
  switch (method) {
    case 1:
      for (i = 1; i < r-1; i++)   for (j = 1; j < c-1; j++) {
        H = (dem[i-1][j] - dem[i+1][j]) / ( 2.0 * cellS);
        G = (dem[i][j+1] - dem[i][j-1]) / ( 2.0 * cellS);
        aspectarr[i][j] = aspectarr_update(atan(H/G), H, G);
      }
      break;
      case 2:
      for (i = 1; i < r-1; i++)   for (j = 1; j < c-1; j++) {
        H = ((dem[i-1][j-1] + 2*dem[i-1][j] + dem[i-1][j+1]) - (dem[i+1][j-1] + 2*dem[i+1][j] + dem[i+1][j+1])) / ( 8.0 * cellS);
        G = ((dem[i-1][j+1] + 2*dem[i][j+1] + dem[i+1][j+1]) - (dem[i-1][j-1] + 2*dem[i][j-1] + dem[i+1][j-1])) / ( 8.0 * cellS);
        aspectarr[i][j] = aspectarr_update(atan(H/G), H, G);
      }
  }
  return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
int slopecalc(float **dem, int r, int c, float cellSize, float noDataValue, float **slope_arr)
{
  int i, j;
  float X, Y, cellS;
  if (!cellSize ) return(FAILURE);
  //Init the first line and first colmun with zeros...
  for (i = 0; i < r; i++)  slope_arr[i][0] = slope_arr[i][c-1] = 0.0;
  for (j = 0; j < c; j++)  slope_arr[0][j] = slope_arr[r-1][j] = 0.0;
  cellS = (float) cellSize;
  for (i = 1; i < r-1; i++)   for (j = 1; j < c-1; j++) {
    if (dem[i][j] == noDataValue || dem[i][j-1] == noDataValue || dem[i][j+1] == noDataValue || dem[i-1][j] == noDataValue || dem[i+1][j] == noDataValue) slope_arr[i][j] = -1;
     else  {
       X = (dem[i][j-1] - dem[i][j+1]) / ( 2.0 * cellS);
       Y = (dem[i-1][j] - dem[i+1][j]) / ( 2.0 * cellS);
       slope_arr[i][j] = fabs (atan ( sqrt ((X * X ) + (Y * Y))) * 360.0/(2.0 * PHI));
    }
  }
  return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
int impact_depth(float R, float Rn, float V, float RockMass, float *dp)
{
  float d, H, rho_soil, Ri, k, B, Nsf, Imp, N;
  float Penetration_depth1, Penetration_depth2;
  d = 2.0 * R;   // d is the diameter of the projectile,
  H = R;             //height impactor nose
  rho_soil = 1200.0 *log(Rn) + 3300.0;  //density of impacted material kg/m3
  Ri = 55000000000.0*pow(Rn, 7);      //intact rock strength/indentation resistance of impacted material (9.10+6 gravel 100.10+6 bedrock):
  k = 0.707 + H/d;
  B = 1.2;        //dimensionless compressibility parameter of the impacted material (varies little for different materials)
  //Input functions
  Nsf = 1.0 /(1.0 + 4*(H /d) *(H /d)); //Nose Sharp Factor
  Imp = (RockMass* V * V) /(float)(Ri*d *d *d);
  N = RockMass /(rho_soil *d *d *d *B *Nsf);
  //Main functions
  Penetration_depth1 = (sqrt ((1 + k *PHI/4.0 *N) /(1.0 + Imp /N) * ((4 *k) /PHI) *Imp)) *d;
  Penetration_depth2 = ((2 /PHI) *N *log((1 + Imp /N) /(1.0 + k *PHI /4.0 *N)) + k) *d;
  if (Penetration_depth1 /d <= k)  *dp = Penetration_depth1;
  else   *dp = Penetration_depth2;
  *dp = Min(*dp, R);
  return (SUCCESS);
}
/*------------------------------------------------------------------------------------*/
int direction_after_rebound(float aspect, float velocity, float *richting)
{
  float dd, dirchange;
  if (aspect > -1)  { //if the block entres a pit (depression)
    if (*richting > 270 && aspect < 90) aspect += 360;
    if (aspect > 270 && *richting < 90)  *richting += 360;
    dd = *richting - aspect;
    if (!dd)  dd = 1;
    if (fabs(dd) > 90) { *richting = fmodf(360 + (*richting - 22.5 + unifRand() *45) ,360.0);}
    else if (velocity > 15)  {
      dirchange = 47 *pow(unifRand(), 2.85);
      *richting = fmodf(360 + (*richting - sign(dd) * dirchange) ,360.0);
    }
    else {
      if (velocity > 10) {
        dirchange = 0.503 *exp(unifRand()*4.63);
        *richting = fmodf(360 + (*richting - sign(dd) * dirchange) ,360.0);
      }
      else {
        dirchange = 0.328 *exp(unifRand() *5.02);
        *richting = fmodf(360 + (*richting - sign(dd) * dirchange) ,360.0);
      }
    }
  }

  return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
int rebound(float slope, float I,int rnin, int indBat, float RockMass, float R, float *V_hor, float *V_vert, float *V_rot, int r, int c,float **SoilParam)
{
	float V, beta, slope_rad, Normal_Depth, randangle;
	float dp, LSA, rgin, rg, Rt_in, Rt, Rn;
	float Vt1, Vt2, Vn1, Vn2, Vrot1, Vrot2;
	int f10000=0,f1000=0,f100=0,f10=0;f100000=0;


	Rn = 0;
	rgin = 0.05;

	// ///////////////////////////////////////////////////////////////////////
	//  ///////////////////////////////////////////////////////////////////////
	if (rnin>=1) {
		Rn = unifRand()*(SoilParam[rnin-1][2]-SoilParam[rnin-1][1]) +SoilParam[rnin-1][1];
		rgin = unifRand()*(SoilParam[rnin-1][4]-SoilParam[rnin-1][3]) +SoilParam[rnin-1][3];
	}

	///////////////////////////////////////////////////////////////////////
	//Custom soil param according to issue type
	if (indBat>0) {
		Rn = unifRand()*(SoilParam[3][2]-SoilParam[3][1]) +SoilParam[3][1];
    rgin = unifRand()*0.075 +0.05;
    if ((indBat-100000)>=0){f100000=1};
		if ((indBat-10000-100000*f100000)>=0){f10000=1};
		if ((indBat-1000-100000*f100000-f10000*10000)>=0){f1000=1};
		if ((indBat-100-100000*f100000-f10000*10000-f1000*1000)>=0) {f100=1};
		if ((indBat-10-100000*f100000-f10000*10000-f1000*1000-f100*100)>=0) {
			f10=1;
			rgin = unifRand()*(SoilParam[3][4]-SoilParam[3][3]) +SoilParam[3][3];}
		if ((indBat-1-100000*f100000-f10000*10000-f1000*1000-f100*100-f10*10)>=0) {
			rgin = unifRand()*(SoilParam[3][4]-SoilParam[3][3]) +SoilParam[3][3];}
	}
	/////////////////////////////////////////////////////////////////////
	slope_rad = slope *PHI /180.0;
	Normal_Depth = 0;
	randangle = 4;
	if (rnin <= 0.43) {
		V = sqrt((*V_vert) *(*V_vert) + (*V_hor) *(*V_hor));
		impact_depth(R, Rn, V, RockMass, &dp);
		beta = atan(fabs(*V_vert) /fabs(*V_hor)) - fabs(slope_rad);
	Normal_Depth = fabs(Min(dp * sin(beta), 1));
	}
	if ((rnin == 0.43 && slope < 25) || (rnin == 0))  randangle=0;
	else {
		if (rnin == 0.28 && slope < 25)  randangle = 2;
		else  if (rnin == 0.23 && slope < 25)  randangle = 1;
	}
	LSA = Max(0, (slope_rad - unifRand()*(randangle *PHI /180.0))); // LSA = slope angle at position of rebound
	//////////////////////////////////////////////////////////////////////////
	//////////////Modification du tirage de la rugosité par TOE ET DUPIRE  22_05_2018
	rg = rgin + Normal_Depth;
	Rt_in = (1.0/(1.0 + rg/R)) *floor(Min(1, 3*R/rg));
	Rt = Rt_in + (-0.1+unifRand()*0.2)*Rt_in;
	Rt = Min(0.9999, Rt);
	Rt = Max(0, Rt);
	Rn = Max(0, Rn);
	if (slope >= 0) {
		Vt1 = (*V_hor)*cos(LSA) - (*V_vert)*sin(LSA);
		Vn1 = (*V_vert)*cos(LSA) + (*V_hor)*sin(LSA);
		Vt2 = sqrt((R*R*((I*(*V_rot)*(*V_rot)) + (RockMass*Vt1*Vt1)) *Rt) /(I + (RockMass*R*R)));
		Vn2 =  -1 *Vn1 *Rn /(1.0 + (fabs(Vn1) /10.0)*(fabs(Vn1)/10.0));
		*V_hor = Vn2 *sin(LSA) + Vt2 *cos(LSA);
		*V_vert = Vn2 *cos(LSA) - Vt2 *sin(LSA);
		Vrot1 = Vt2 /R;
		Vrot2 = *V_rot + 2.0*(Vt1 - Vt2)/(5.0*R);
		*V_rot = Min(Vrot1, Vrot2) + unifRand()*fabs(Vrot1 - Vrot2);
		return (SUCCESS);
	}
	if (slope > -30) {
		Vt2 = (*V_rot) *R *Rt;
		*V_rot = Rt *Vt2 /R;
		*V_hor = Vt2 *cos(slope_rad);
		*V_vert = Vt2 *sin(slope_rad);
		return (SUCCESS);
	}
	*V_hor = 0.0;
	*V_vert = 0.0;
	*V_rot = (*V_rot) * Rt;
	return (SUCCESS);

}
/*------------------------------------------------------------------------------------*/
int calc_new_pos(float maxdist, float richting, float cellsize, int start_r, int start_c, float Xcoor_start, float Ycoor_start, float impact,
              float *X, float *Y, int *r, int *c, float *Xcoor, float *Ycoor,float *new_distance )
{
  float Xout, Yout, halfacell = (float)cellsize/2.0;
  int addx = 0, addy = 0;
  if (*new_distance >= maxdist && impact == 0 && fabs(*Y) >= halfacell && fabs(*X) >= halfacell) {
    float virtual_distance = 0.1;
    Xout = *X + (cos(richting *PHI /180.0)) *virtual_distance;
    Yout = *Y + (sin(richting *PHI /180.0)) *virtual_distance;
    *X = (round(Xout *100.0))/100.0;
    *Y = (round(Yout *100.0))/100.0;
    if (fabs(*X) >= halfacell) {
      addx = 1;
      *X = -(*X);
    }
    if (fabs(*Y) >= halfacell) {
      addy = 1;
      *Y = -(*Y);
    }
    *r +=  sign(*Y)*addy;
    *c -=  sign(*X)*addx;
    *X = sign(*X) *halfacell;
    *Y = sign(*Y) *halfacell;
  }
  else {
    if (*new_distance >= maxdist && impact == 0) {
      Xout = *X + (cos(richting *PHI /180.0)) *(*new_distance);
      Yout = *Y + (sin(richting *PHI /180.0)) *(*new_distance);
      *X = (round(Xout*100.0))/100.0;
      *Y = (round(Yout*100.0))/100.0;
      if (fabs(*X) >= halfacell) {
        addx = 1;
        *X = -(*X);
      }
      if (fabs(*Y) >= halfacell) {
        addy = 1;
        *Y = -(*Y);
      }
      *r += sign(*Y)*addy;
      *c -= sign(*X)*addx;
    }
    else {
      Xout = *X + (cos(richting *PHI /180.0)) *(*new_distance);
      Yout = *Y + (sin(richting *PHI /180.0)) *(*new_distance);
      *X = (round(Xout *100.0)) /100.0;
      *Y = (round(Yout *100.0)) /100.0;
    }
  }
  *new_distance = round(*new_distance *100.0)/100.0;
  *Xcoor = Xcoor_start + ((*c - start_c) *cellsize) + *X;
  *Ycoor = Ycoor_start + ((start_r - *r) *cellsize) + *Y;
  return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
