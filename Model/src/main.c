  /*-----------------------------------------------------------------------------------------------*/
  /* Main funtion of Sylvarock. Version adapted in 2018 by David Toe base on RF3D script from 2012 */
  /*                     remasterized in 2022 by Sylvain Dupire                                    */
  /*      Mainly contain functions to retrieve informations from the command line                  */
  /*-----------------------------------------------------------------------------------------------*/


#include "SylvaRock.h"
//Message info print for the different stage of the program advace
void on_updateProgress(int i) {
	if(i == -1){ printf("Loading files...\n");}
	else if(i == -2) { printf("Files were loaded successfuly.\nSimulation started...\n");}
	else if(i == -3) { printf("Saving Files.......Please wait!!!\n");}
	else{ i=-4;}
}
/*------------------------------------------------------------------------------------*/
static int argindex(int argc, char *argv[], char *name, int n)
{
  int i;
  for (i = 1; i < argc-n; i++) if (!strcmp(name,argv[i])) return(i);
  return(0);
}
/*------------------------------------------------------------------------------------*/
static void argremove(int *pargc, char *argv[], int i, int n)
{
  int j;
  *pargc -= n+1;
  for (j = i; j < *pargc; j++) argv[j] = argv[j+n+1];
}
/*------------------------------------------------------------------------------------*/
char *gettarg(int *pargc, char *argv[], char *name, char *sarg)
{ 
  /*---------------------------------------------------------------*/
  /* Extracts a string parameter from the argument list with alloc */
  /*---------------------------------------------------------------*/
  int i;
  char *s;
  if ((i = argindex(*pargc,argv,name,1))) {
    s = strdup(argv[i+1]);
    argremove(pargc,argv,i,1);
  } else {
    s = sarg ? strdup(sarg) : NULL;
  }
  return(s);
}
/*------------------------------------------------------------------------------------*/
int getfarg(int *pargc, char *argv[], char *name, float fdef, float *pfarg)
{
  /*---------------------------------------------------*/
  /* Extracts a float parameter from the argument list */
  /*---------------------------------------------------*/
  int i;
  *pfarg = fdef;
  if ((i = argindex(*pargc,argv,name,1))) {
    if (sscanf(argv[i+1],"%f",pfarg) != 1) {
      fprintf(stderr,"ERROR: %s option expects a valid float\n",name);
      return(FAILURE);
    }
    argremove(pargc,argv,i,1);
  }
  return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
int getiarg(int *pargc, char *argv[], char *name, int idef, int *piarg)
{
  /*------------------------------------------------------*/
  /* Extracts an integer parameter from the argument list */
  /*------------------------------------------------------*/
  int i;
  *piarg = idef;
  if ((i = argindex(*pargc,argv,name,1))) {
    if (sscanf(argv[i+1],"%d",piarg) != 1) {
      fprintf(stderr,"ERROR: %s option expects a valid integer\n",name);
      return(FAILURE);
    }
    argremove(pargc,argv,i,1);
  }
  return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
void	USAGE()
{ 
  printf("USAGE : \n");
  printf("Sylvarock ....\n");
  printf("\t-nthr NB_THREADS: the number of cores (processors) default 1\n");
	printf("\t-path PATH: the folder where the data to process are\n");
	printf("\t-nsim NR_SIM: the number of simulations (default 1)\n");
	printf("\t-inifall INI_FALL: float number indicates the initial fall value (default 0)\n");
  printf("\t-vol VOL: float number indicates the volume of the blocks\n");
}
int main(int argc,char* argv[])
{
	int nbThreads, nr_sim;
	float ini_fall,ini_volume;
	char *textbox_messages, *path;
	struct Image image;
  //Slice and extract the informations from the command line
	if(strcmp(argv[1],"--h")==0 || strcmp(argv[1],"-h")==0) { USAGE (); return 0; }
	if ((path = gettarg(&argc, argv, "-path", "../")) == NULL) exit(1);
	if (getiarg(&argc,argv,"-nthr", 1, &nbThreads) == FAILURE) exit(1);
	if (getiarg(&argc,argv,"-nsim", 1, &nr_sim) == FAILURE) exit(1);
	if (getfarg(&argc,argv,"-inifall", 0, &ini_fall) == FAILURE) exit(1);
	if (getfarg(&argc,argv,"-vol", 0., &ini_volume) == FAILURE) exit(1);
	if( SylvaRock(path, nbThreads, nr_sim,ini_volume, ini_fall, &textbox_messages, &image, on_updateProgress)==FAILURE) {printf("Error!!!\n");return(1);}
	printf("%s\n",textbox_messages);
	if(textbox_messages) free(textbox_messages);
	printf("\n%d   %d\nDONE.\n", MaxUsedMemory(),UsedMemory());
	return(0);
}
