#include "tool_tens.h"
int UsedMem=0, MaxUsedMem=0;
int UsedMemory() {
  return(UsedMem);
}
/*------------------------------------------------------------------------------------*/
int MaxUsedMemory() {
  return(MaxUsedMem);
}
/*------------------------------------------------------------------------------------*/
void getLower(char *str) {
  int i, l = strlen(str);
  for(i = 0; i < l; i++) str[i] = tolower(str[i]);
}
/*------------------------------------------------------------------------------------*/
float unifRand()
{
    return (rand()/(float)(RAND_MAX));
}
/*------------------------------------------------------------------------------------*/
char *strcat2(const char *s1, const char *s2)
{
  char *s;
  if ((s = (char *) malloc(strlen(s1)+strlen(s2)+1)) == NULL) return(NULL);
  strcpy(s,s1); strcat(s,s2);
  return(s);
}
/*------------------------------------------------------------------------------------*/
char *strcat3(const char *s1, const char *s2, const char *s3)
{
  char *s;
  if ((s = (char *) malloc(strlen(s1)+strlen(s2)+strlen(s3)+1)) == NULL) return(NULL);
  strcpy(s,s1); strcat(s,s2); strcat(s,s3);
  return(s);
}
/*------------------------------------------------------------------------------------*/
void *malloc1a(int n0, int size)
{
  void *v1;
  if (n0 <= 0) {fprintf(stderr,"Error in malloc1a: n0 <= 0\n"); return(NULL);}
  if (!(v1 = (void *) calloc(n0,size))) {
    fprintf(stderr,"Error: malloc1a(%d,%d) failed\n",n0,size);
    return(NULL);
  }
  UsedMem += n0*size;
  if (UsedMem > MaxUsedMem) MaxUsedMem = UsedMem;
  return(v1);
}
/*------------------------------------------------------------------------------------*/
void free1a(void *v1, int n0, int size)
{
  free(v1);
  v1=0x0;
  UsedMem -= n0*size;
}
/*------------------------------------------------------------------------------------*/
void *malloc2a(int n0, int n1, int size)
{
  int i0;
  char *v1;
  void **v2;
  if (n0 <= 0) {fprintf(stderr,"Error in malloc2a: n0 <= 0\n"); return(NULL);}
  if (n1 <= 0) {fprintf(stderr,"Error in malloc2a: n1 <= 0\n"); return(NULL);}
  if (!(v2 = (void **) malloc(n0*sizeof(void *)))
   || !(v1 = (char *) malloc(n0*n1*size))) {
    fprintf(stderr,"Error: malloc2a(%d,%d,%d) failed\n",n0,n1,size);
    return(NULL);
  }
  for (i0 = 0; i0 < n0; i0++) v2[i0] = v1+i0*n1*size;
  UsedMem += n0*sizeof(void *)+n0*n1*size;
  if (UsedMem > MaxUsedMem) MaxUsedMem = UsedMem;
  return(v2);
}
/*------------------------------------------------------------------------------------*/
void free2a(void *v2, int n0, int n1, int size)
{
  free(*((char **) v2));
  free(v2);
  v2=0x0;
  UsedMem -= n0*sizeof(void *)+n0*n1*size;
}
/*------------------------------------------------------------------------------------*/
float **zero(int m, int n) {
  int i, j;
  float **result;
  if (!(result = (float**) malloc2a(m, n, sizeof(float))))  return(FAILURE);
    for (i = 0; i < m; i++) for (j = 0; j < n; j++) result[i][j] = 0.0;
  return result;
}
/*------------------------------------------------------------------------------------*/
int **zero_i(int m, int n) {
  int i, j;
  int **result;
  if (!(result = (int**) malloc2a(m, n, sizeof(int))))  return(FAILURE);
    for (i = 0; i < m; i++) for (j = 0; j < n; j++) result[i][j] = 0;
  return result;
}
/*------------------------------------------------------------------------------------*/
int fCheckOpen(const char *name, char *mode, FILE **fp)
{
   if (!(*fp = fopen(name,mode)))  return(FAILURE);
   return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
int hdrload_f (const char *path, char *file_in, HEADER *head, float VAL, float ***dem)
{
  int i, j;
  float val;
  char str[100], *pathFile;
  pathFile=strcat2(path, file_in);
  FILE *file;
  if (fCheckOpen(pathFile, "r", &file) == FAILURE)  { free (pathFile); return(FAILURE);}
  free (pathFile);
  fscanf (file,"%s %d\n",str,&(head->cols));
  fscanf (file,"%s %d\n", str, &(head->rows));
  fscanf (file,"%s %f\n", str, &(head->x));
  getLower(str);
  if (! strcmp(str, "xllcorner")) head->isCorner = 1;
  fscanf (file,"%s %f\n",str, &(head->y));
  fscanf (file,"%s %f\n",str, &(head->cellSize));
  fscanf (file,"%s %f\n",str, &(head->noDataValue));
  if (! (*dem = (float **) malloc2a( head->rows, head->cols, sizeof(float ))))  return(FAILURE);
  for (i = 0; i < head->rows; i++) for (j = 0; j < head->cols; j++) {
    if( fscanf (file,"%f ", &val) < 1 ) return(FAILURE);
    (*dem)[i][j] = (val >= 0.0) ? val : VAL;
  }
  fclose(file);
  return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
int hdrload_i (const char *path, char *file_in, HEADER *head, float VAL, int ***dem)
{
  int i, j;
  int val;
  char str[100], *pathFile;
  pathFile=strcat2(path, file_in);
  FILE *file;
  if (fCheckOpen(pathFile, "r", &file) == FAILURE)  { free (pathFile); return(FAILURE);}
  free (pathFile);
  fscanf (file,"%s %d\n",str,&(head->cols));
  fscanf (file,"%s %d\n", str, &(head->rows));
  fscanf (file,"%s %f\n", str, &(head->x));
  getLower(str);
  if (! strcmp(str, "xllcorner")) head->isCorner = 1;
  fscanf (file,"%s %f\n",str, &(head->y));
  fscanf (file,"%s %f\n",str, &(head->cellSize));
  fscanf (file,"%s %f\n",str, &(head->noDataValue));
  if (! (*dem = (int **) malloc2a( head->rows, head->cols, sizeof(int ))))  return(FAILURE);
  for (i = 0; i < head->rows; i++) for (j = 0; j < head->cols; j++) {
    if( fscanf (file,"%d ", &val) < 1 ) return(FAILURE);
    (*dem)[i][j] = (val >= 0.0) ? val : VAL;
  }
  fclose(file);
  return(SUCCESS);
}

/*------------------------------------------------------------------------------------*/
int load_soilParam (const char *path, char *file_in, float VAL,float ***SoilParam)
{
  int i, j;
  float val;
  char *pathFile;
  pathFile=strcat2(path, file_in);
  FILE *file;
  if (fCheckOpen(pathFile, "r", &file) == FAILURE)  { free (pathFile); return(FAILURE);}
  free (pathFile);  
  fscanf(file, "%*[^\n]\n");
  if (! (*SoilParam = (float **) malloc2a( 8, 5, sizeof(float ))))  return(FAILURE);  
  for (i = 0; i < 8; i++) for (j = 0; j < 5; j++) {  
    if( fscanf (file,"%f;", &val) < 1 ) return(FAILURE);
    (*SoilParam)[i][j] = (val >= 0.0) ? val : VAL;
  }
  fclose(file);
  return(SUCCESS);
}
/*------------------------------------------------------------------------------------*/
int save_asciigrd (const char *path, char *file_in, HEADER *head, float **dem)
{
  int i,j;
  char *str, *pathFile;
  pathFile=strcat2(path, file_in);
  FILE *file;
  if (fCheckOpen(pathFile, "w", &file) == FAILURE)  { free (pathFile); return(FAILURE);}
  free (pathFile);
  //write the header parameters first
  fprintf (file, "NCOLS\t %d\n", head->cols );
  fprintf (file, "NROWS\t %d\n", head->rows );
  str = (head->isCorner) ? strdup("CORNER") : strdup("CENTER");
  fprintf (file, "XLL%s\t %.2f\n", str, head->x );
  fprintf (file, "YLL%s\t %.2f\n", str, head->y );
  free(str);
  fprintf (file, "CELLSIZE\t %f\n", head->cellSize );
  fprintf (file, "NODATA_VALUE\t %.2f\n", head->noDataValue );
  //write the dem matrix
  for (i = 0; i < head->rows; i++)  {
    fprintf (file, "%.2f", dem[i][0]);
    for (j = 1; j < head->cols; j++)  fprintf (file, " %.4f", dem[i][j]);
    fprintf(file,"\n");
  }
  fclose(file);
  return(SUCCESS);
} 
/*------------------------------------------------------------------------------------*/
int save_asciigrd_i (const char *path, char *file_in, HEADER *head, int **dem)
{
  int i,j;
  char *str, *pathFile;
  pathFile=strcat2(path, file_in);
  FILE *file;
  if (fCheckOpen(pathFile, "w", &file) == FAILURE)  { free (pathFile); return(FAILURE);}
  free (pathFile);
  //write the header parameters first
  fprintf (file, "NCOLS\t %d\n", head->cols );
  fprintf (file, "NROWS\t %d\n", head->rows );
  str = (head->isCorner) ? strdup("CORNER") : strdup("CENTER");
  fprintf (file, "XLL%s\t %.2f\n", str, head->x );
  fprintf (file, "YLL%s\t %.2f\n", str, head->y );
  free(str);
  fprintf (file, "CELLSIZE\t %f\n", head->cellSize );
  fprintf (file, "NODATA_VALUE\t %d\n", (int)head->noDataValue );
  //write the dem matrix
  for (i = 0; i < head->rows; i++)  {
    fprintf (file, "%d", dem[i][0]);
    for (j = 1; j < head->cols; j++)  fprintf (file, " %d", dem[i][j]);
    fprintf(file,"\n");
  }
  fclose(file);
  return(SUCCESS);
}


