# -*- coding: utf-8 -*-
"""
Software: SylvaRock
File: 0_Sylvarock_input_formatting_server.py
Copyright (C) Sylvain DUPIRE - INRAE 2022
Authors: Sylvain DUPIRE
Contact: sylvain.dupire@inrae.fr
Version: 1.0
Date: 2022/06/2
License :  GNU-GPL V3
"""

import numpy as np
import os,gc,shutil,time
from osgeo import gdal,ogr,osr
import psutil
from shapely.geometry import Polygon
from shapely.wkt import loads

##############################################################################
### Params
##############################################################################
Ws = "/mnt/DATA/SylvaRock/"
ResDir = '/mnt/DATA/SylvaRock/test_dep/'

Area_file = ""
Dtm_file = '/media/sylvain/Ext_SDupire/0_Donnees/IGN_Geo/MNT/france_25m_2154_mnt.tif'
OSO_file = '/media/sylvain/Ext_SDupire/0_Donnees/OSO/OSO_20210101_RASTER_V1-0/DATA/OCS_2021.tif'
FFr_file = '/media/sylvain/Ext_SDupire/0_Donnees/FFRandonnee/FFrandonnee.shp'
BDTopo_Source = '/media/sylvain/Ext_SDupire/0_Donnees/IGN_Geo/BDTopo/2019/Zips/'
BDForet_Source =  '/media/sylvain/Ext_SDupire/0_Donnees/IFN/BDForet/v2_France/BD_Foret_propre/'
BD_Departement = '/media/sylvain/Ext_SDupire/0_Donnees/IGN_Geo/Limite_admin_France/departement.shp'
sylvarock_folder = '/mnt/DATA/SylvaRock/'

# Ws = "/home/dupire/SylvaRock/"
# ResDir = Ws+'Results/' 

# Dtm_file = Ws+ 'SIG/MNT/france_25m_2154_mnt.tif' 
# OSO_file = Ws+ 'SIG/OSO/OCS_2021.tif'
# FFr_file = Ws+ 'SIG/FFRandonnee/FFrandonnee.shp'
# BDTopo_Source = Ws+ 'SIG/BDTopo/'
# BDForet_Source =  Ws+ 'SIG/BD_Foret_Propre/'
# BD_Departement = Ws+'SIG/departement.shp'

### Raster to align cell on : 
Extent_to_stick_file = Dtm_file 
Csize = 25  

### Release method
release_method = 'pente'  #'lidar' pour seuil nb cellule,
                          #'pente' pour seuil de pente
                          #'2pente' 2 seuils un bas un haut
                          #'shp' pour une couche shapefile
## If method Lidar
Release_Lidar = 50         #seuil de pente depart Lidar
nb_cell_dep = 26           #nb de cellule pour considerer une zone de depart
Dtm_Lidar_file = Ws+'SIG/MNT/Mnt_lidar_1m.tif' #MNT lidar

## If method seuil de pente
Release_Sl_h = 42          #Seuil de pente haut (non inclus) depart direct sur mnt
Release_Sl_b = 28          #Seuil de pente bas (inclus) depart direct sur mnt
### For using existing release shapefile
Release_file = None

### For manual input extent
xmin = None
xmax = None
ymin = None
ymax = None

##############################################################################
### GIS functions
##############################################################################
def get_all_features_id(BD_Departement,field_name="INSEE_DEP"):  
    """
    Get all DEP_COM from national Departement gis file

    Parameters
    ----------
    BD_Departement : Complete path to commune gis file
    field_name : name of the field to get codes

    Returns
    -------
    values_list : list of all DEP_COM values

    """
    source_ds = ogr.Open(BD_Departement)
    layer = source_ds.GetLayer()
    values_list =[]
    for feature in layer:    
        values_list.append(feature.GetField(field_name))
    source_ds.Destroy()
    return values_list


def get_target_area_extent(Area_file,Csize,xmin,xmax,ymin,ymax,
                           Extent_to_stick_file,buff=1000):
    """
    Convert vector to raster aligned on hStick_Extent. 

    Paramete source_ds = ogr.Open(Area_file)
        source_layer = source_ds.GetLayer()   
        xmin,xmax,ymin,ymax = source_layer.GetExtent()  
        poly = []
        geom = source_layer[0].GetGeometryRef()
        geombuf = geom.Buffer(buff)rs
    ----------
    Area_file : Complete path to the area vector file
    Csize : Pixel resoltion of hdf5 rasters
    Extent_to_stick_file : Raster file to align cell on 
    buff : buffer around area in m

    Returns
    -------
    Area : Bool area with True value on the Area
    Extent_a : Restricted extent of the area (not used, usefull for checks)    

    """
    #Get Extent of the raster file
    names,values,src_proj,Extent = raster_get_info(Extent_to_stick_file)
    
    #Get extent of the area   
    if Area_file is not None:
        source_ds = ogr.Open(Area_file)
        source_layer = source_ds.GetLayer()   
        xmin,xmax,ymin,ymax = source_layer.GetExtent()          
        i=1
        for feat in source_layer:
            geom = feat.GetGeometryRef()
            i+=1
        geombuf = geom.Buffer(buff)
        poly_area = loads(geombuf.ExportToWkt())    
        source_ds=None
        
    #Define the extent of rasterization and stick to rast cells
    xmin_a = Extent[0]+int((int(xmin/10)*10-buff-Extent[0])/Csize)*Csize
    ncols = int(buff/Csize+int((xmax-xmin_a)/Csize))
    ymin_a = Extent[2]+int((int(ymin/10)*10-buff-Extent[2])/Csize)*Csize
    nrows = int(buff/Csize+int((ymax-ymin_a)/Csize))
    Extent_a = [xmin_a,xmin_a+ncols*Csize,ymin_a,ymin_a+Csize*nrows]    

    Area = shapefile_to_int8array(Area_file,Extent_a,Csize)==1
    
    if Area_file is None:
        poly_area = Polygon([(Extent_a[0],Extent_a[2]), 
                             (Extent_a[0],Extent_a[3]), 
                             (Extent_a[1],Extent_a[3]), 
                             (Extent_a[1],Extent_a[2])])
     
    return Extent_a,nrows,ncols,Area,poly_area

def ogrWkt2Shapely(input_shape):
    # this throws away the other attributes of the feature, but is 
    # sufficient in this use case
    shapely_objects=[]
    shp = ogr.Open(input_shape)
    lyr = shp.GetLayer()
    for n in range(0, lyr.GetFeatureCount()):
        feat = lyr.GetFeature(n)
        wkt_feat = loads(feat.geometry().ExportToWkt())
        shapely_objects.append(wkt_feat)
    return shapely_objects


def check_reg_in(filename,poly_area):
    """
    Check if file intersect the target extent

    Parameters
    ----------
    filename : complete path to the file
    poly_coord : Extent of the targer area

    Returns
    -------
    test : return 1 if intersection 0 otherwise

    """
    shapely_objects = ogrWkt2Shapely(filename)
    
    
    test=0
    for Poly in shapely_objects:
        if Poly.intersects(poly_area):
            test=1
            break    
    return test

def check_dep_in(BD_Departement,poly_area):
    """
    Check if file intersect the target extent

    Parameters
    ----------
    filename : complete path to the file
    poly_coord : Extent of the targer area

    Returns
    -------
    test : return 1 if intersection 0 otherwise

    """
    Dep_list=[]
    shp = ogr.Open(BD_Departement)
    lyr = shp.GetLayer()
    for n in range(0, lyr.GetFeatureCount()):
        feat = lyr.GetFeature(n)
        wkt_feat = loads(feat.geometry().ExportToWkt())
        if wkt_feat.intersects(poly_area):
            Dep_list.append((feat.GetField('INSEE_DEP')).casefold())

    return Dep_list


def get_targetDBTOPO_BDFORET(BDTopo_Source,BDForet_Source,BD_Departement,poly_area):
    BDTopo = []
    for path, subdirs, files in os.walk(BDTopo_Source):        
        for name in files:
            filename, file_extension = os.path.splitext(name)
            if filename=="EMPRISE" and file_extension==".shp":  
                if check_reg_in(os.path.join(path, name),poly_area):
                    path2 =os.path.split(path)[0]
                    path2 =os.path.split(path2)[0]
                    path2 =os.path.split(path2)[0]
                    for path3, subdirs2, files2 in os.walk(path2+'/'):
                        for name2 in files2:
                            filename2, file_extension2 = os.path.splitext(name2)
                            if filename2=="TRANSPORT" and file_extension2==".md5":  
                                word = path3[-30:]
                                if word.find(path2[-28:-18])>-1:
                                    BDTopo.append(path3+'/')   
    
    Dep_list = check_dep_in(BD_Departement,poly_area)
    
    BDForet = []
    for path, subdirs, files in os.walk(BDForet_Source):
        for name in files:
            filename, file_extension = os.path.splitext(name)
            if file_extension==".shp":
                if filename[10:12].casefold() in Dep_list:                
                    BDForet.append(os.path.join(path, name))    

    return BDTopo,BDForet                        

        
def shapefile_to_int8array(file_name,Extent,Csize):
    """
    Create a numpy int8 array from shapefile 
    ----------
    Parameters
    ----------
    file_list:              string      List of .shp file to rasterize
    Extent:                 list        Extent of the area : [xmin,xmax,ymin,ymax]
    Csize:                  int, float  Cell resolution of the area  

    Returns
    -------
    mask_array :            ndarray int32
    """
    #Get raster dimension
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)        
    #Create obstacle raster
    Array = np.zeros((nrows,ncols),dtype=np.int8)      
    # Get shapefile info
    source_ds = ogr.Open(file_name)
    source_layer = source_ds.GetLayer()    
    source_srs = source_layer.GetSpatialRef()
    source_type = source_layer.GetGeomType()
    # Create copy
    target_ds1 = ogr.GetDriverByName("Memory").CreateDataSource("")
    layerName = "shp"
    layer = target_ds1.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('Transfo', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('Transfo',1)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Initialize raster
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)         
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds, [maskvalue], layer,options=["ATTRIBUTE=Transfo","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        Array = target_ds.GetRasterBand(1).ReadAsArray()
    target_ds1.Destroy()
    source_ds.Destroy()
    return Array

def raster_get_info(in_file_name):
    """
    Get info on raster dataset

    Parameters
    ----------
    in_file_name : any raster file with GDAL compatible extension

    """
    source_ds = gdal.Open(in_file_name)    
    src_proj = osr.SpatialReference(wkt=source_ds.GetProjection())
    src_ncols = source_ds.RasterXSize
    src_nrows = source_ds.RasterYSize
    xmin,Csize_x,a,ymax,b,Csize_y = source_ds.GetGeoTransform()
    ymin = ymax+src_nrows*Csize_y
    nodata = source_ds.GetRasterBand(1).GetNoDataValue()
    names = ['ncols', 'nrows', 'xllcorner',
             'yllcorner', 'cellsize','NODATA_value']
    values = [src_ncols,src_nrows,xmin,ymin,Csize_x,nodata]
    Extent = [xmin,xmin+src_ncols*Csize_x,ymin,ymax]
    return names,values,src_proj,Extent


def load_raster(raster_file):
    """
    Load a raster and convert it to numpy array    
    
    Parameters
    ----------
    raster_file : any raster file with GDAL compatible extension
    
    Returns
    -------
    Array
        Numpy array in the desired formatting   
    """
    dataset = gdal.Open(raster_file,gdal.GA_ReadOnly)    
    band = dataset.GetRasterBand(1)
    band.format = format(gdal.GetDataTypeName(band.DataType))
    band.nodata = band.GetNoDataValue()      
    Array = band.ReadAsArray()
    Array[np.isnan(Array)]=band.nodata
    dataset.FlushCache()      
    return Array


def get_Slope(Dtm_file,slope_format = 'degree'):
    """
    Compute slope raster from DEM file

    Parameters
    ----------
    Dtm_file : any raster file with GDAL compatible extension
    slope_format : 'degree' or 'percent'
        DESCRIPTION. The default is 'degree'.

    Returns
    -------
        Slope Array
    """
    a=gdal.DEMProcessing('slope', Dtm_file, 'slope',slopeFormat=slope_format,
                         computeEdges=True,format='MEM')
    return a.GetRasterBand(1).ReadAsArray()


def get_Aspect(Dtm_file):
    """
    Compute aspect raster from DEM file

    Parameters
    ----------
    Dtm_file : any raster file with GDAL compatible extension

    Returns
    -------
        Aspect Array
    """
    a=gdal.DEMProcessing('aspect', Dtm_file, 'aspect',
                         computeEdges=True,format='MEM')
    return a.GetRasterBand(1).ReadAsArray()


def get_proj_from_shp(shp_file):
    source_ds = ogr.Open(shp_file)
    source_layer = source_ds.GetLayer()    
    proj = source_layer.GetSpatialRef()
    wkt_proj = proj.ExportToWkt()
    return wkt_proj,proj


def ArrayToGtiff(Array,file_name,Extent,nrows,ncols,Csize,wkt_proj,
                 nodata_value,raster_type='INT32'):
    """
    Create Tiff raster from numpy array   
    ----------
    Parameters
    ----------
    Array:             np.array    Array name
    file_name:         string      Complete name of the output raster
    Extent:            list        Extent of the area : [xmin,xmax,ymin,ymax]
    nrows:             int         Number of rows in the array
    ncols:             int         Number of columns in the array
    Csize:             int, float  Cell resolution of the array  
    wkt_proj:          string      Spatial projection
    nodata_value:      int, float  Value representing nodata in the array
    raster_type:       string      'INT32' (default),'UINT8','UINT16',
                                   'FLOAT32','FLOAT16'
    """
    xmin,xmax,ymin,ymax=Extent[0],Extent[1],Extent[2],Extent[3]
    xres=(xmax-xmin)/float(ncols)
    yres=(ymax-ymin)/float(nrows)
    geotransform=(xmin,xres,0,ymax,0, -yres)
    if raster_type=='INT32':
        #-2147483648 to 2147483647
        DataType = gdal.GDT_Int32    
    elif raster_type=='UINT8':
        #0 to 255
        DataType = gdal.GDT_Byte
    elif raster_type=='UINT16':
        #0 to 65535    
        DataType = gdal.GDT_UInt16
    elif raster_type=='INT16':
        #-32768 to 32767 
        DataType = gdal.GDT_Int16
    elif raster_type=='FLOAT32':
        #Single precision float: sign bit, 8 bits exponent, 23 bits mantissa
        DataType = gdal.GDT_Float32
    elif raster_type=='FLOAT16':
        #Half precision float: sign bit, 5 bits exponent, 10 bits mantissa
        DataType = gdal.GDT_Float16
    target_ds = gdal.GetDriverByName('GTiff').Create(file_name+'.tif', int(ncols), int(nrows), 1, DataType)
    target_ds.SetGeoTransform(geotransform)
    target_ds.SetProjection(wkt_proj)
    target_ds.GetRasterBand(1).WriteArray( Array )
    target_ds.GetRasterBand(1).SetNoDataValue(nodata_value)
    target_ds.GetRasterBand(1).GetStatistics(0,1)
    target_ds.FlushCache()


def create_release_gis(Releases_Array,ResFold,wkt_proj,proj,Extent,Csize,
                       save_tif,save_shp):   
    """
    Create raster tif of release area and/or shapefile of release area.
    """                                 
    #Save release array to tif
    nrows,ncols = Releases_Array.shape
    ArrayToGtiff(Releases_Array,ResFold+"Release",Extent,nrows,ncols,
                 Csize,wkt_proj,0,raster_type='UINT8')    
    if save_shp:
        type_mapping = { gdal.GDT_Byte: ogr.OFTInteger,
                 gdal.GDT_UInt16: ogr.OFTInteger,   
                 gdal.GDT_Int16: ogr.OFTInteger,    
                 gdal.GDT_UInt32: ogr.OFTInteger,
                 gdal.GDT_Int32: ogr.OFTInteger,
                 gdal.GDT_Float32: ogr.OFTReal,
                 gdal.GDT_Float64: ogr.OFTReal,
                 gdal.GDT_CInt16: ogr.OFTInteger,
                 gdal.GDT_CInt32: ogr.OFTInteger,
                 gdal.GDT_CFloat32: ogr.OFTReal,
                 gdal.GDT_CFloat64: ogr.OFTReal}
        layer_name = "Releases"
        #Convert release tif to shp
        ds = gdal.Open(ResFold+"Release.tif")
        srcband = ds.GetRasterBand(1)
        drv = ogr.GetDriverByName("ESRI Shapefile")
        dst_ds = drv.CreateDataSource( ResFold+layer_name+".shp")
        dst_layer = dst_ds.CreateLayer(layer_name, proj , ogr.wkbPolygon)
        raster_field = ogr.FieldDefn('RELEASE', type_mapping[srcband.DataType])
        dst_layer.CreateField(raster_field)
        gdal.Polygonize( srcband, None, dst_layer, 0, [], callback=None)
        ds.FlushCache() 
        for feat in dst_layer:
            i=feat.GetField("RELEASE")  
            if i==0:
                dst_layer.DeleteFeature(feat.GetFID())            
            dst_layer.SetFeature(feat)        
        # Cleanup
        dst_ds.Destroy()  


def auto_identify_release(Sl_arr,Area,Sl_threshold=50,Extent=None,Csize=None,
                          save_tif=False,save_shp=False,
                          wkt_proj=None,proj=None,ResFold=None):
    """
    Identify release area from slope raster according to slope threshold.
    Compute a list of release cells according to raster coordinates

    Parameters
    ----------
    Sl_arr : Array of slope (degree)
    Sl_threshold : Slope threshold in degree. The default is 50.
    save_tif : build a raster tifff containting only release areas
    save_shp : build a shapefile containting only release areas
    shp_prj : projection use for shapefile
    ResFold : Folder where to save shapefile result

    Returns
    -------
        Releases : List of release areas
    """
    Releases_Array = Area*np.int16(Sl_arr>=Sl_threshold)
    if save_tif or save_shp:
        create_release_gis(Releases_Array,ResFold,wkt_proj,proj,Extent,Csize,
                           save_tif,save_shp)
        if not save_tif:
            try:
                os.remove(ResFold+"Releases.tif")
            except:
                pass
    
    return Releases_Array

def auto_identify_release2(Sl_arr,Area,Sl_threshold_b=28,
                           Sl_threshold_h=42, Extent=None,Csize=None,
                           save_tif=False,save_shp=False,
                           wkt_proj=None,proj=None,ResFold=None):
    """
    Identify release area from slope raster according to slope threshold.
    Compute a list of release cells according to raster coordinates

    Parameters
    ----------
    Sl_arr : Array of slope (degree)
    Sl_threshold : Slope threshold in degree. The default is 50.
    save_tif : build a raster tifff containting only release areas
    save_shp : build a shapefile containting only release areas
    shp_prj : projection use for shapefile
    ResFold : Folder where to save shapefile result

    Returns
    -------
        Releases : List of release areas
    """
    Releases_Array = Area*np.int16(Sl_arr>=Sl_threshold_b)*np.int16(Sl_arr<Sl_threshold_h)
    if save_tif or save_shp:
        create_release_gis(Releases_Array,ResFold,wkt_proj,proj,Extent,Csize,
                           save_tif,save_shp)
        if not save_tif:
            try:
                os.remove(ResFold+"Releases.tif")
            except:
                pass
    
    return Releases_Array

def shapefile_to_np_array(file_name,Extent,Csize,attribute_name,
                          order_field=None,order=None):
    """
    Convert shapefile to numpy array
    ----------
    Parameters
    ----------
    file_name:              string      Complete name of the shapefile to convert
    Extent:                 list        Extent of the array : [xmin,xmax,ymin,ymax]
    Csize:                  int, float  Cell resolution of the output array
    attribute_name:         string      Attribute name of the field used for rasterize
    order_field (optional): string      Attribute name of the field used to order the rasterization
    order (optional):       string      Sorting type : 'ASC' for ascending or 'DESC' descending

    Returns
    -------
    mask_array :            ndarray int32
    ----------
    Examples
    --------
    >>> import ogr,gdal
    >>> import numpy as np
    >>> mask_array = shapefile_to_np_array("Route.shp",[0,1000,0,2000],5,"Importance","Importance",'ASC')
    """
    #Recupere les dimensions du raster ascii
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows,ncols = int((ymax-ymin)/float(Csize)+0.5),int((xmax-xmin)/float(Csize)+0.5)
    # Get information from source shapefile
    orig_data_source = ogr.Open(file_name)
    source_ds = ogr.GetDriverByName("Memory").CopyDataSource(orig_data_source, "")
    source_layer = source_ds.GetLayer()
    if order:
        source_layer_ordered = source_ds.ExecuteSQL('SELECT * FROM '+str(source_layer.GetName())+' ORDER BY '+order_field+' '+order)
    else:source_layer_ordered=source_layer
    source_srs = source_layer.GetSpatialRef()
    # Initialize the new memory raster
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)    
    target_ds = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows), 1, gdal.GDT_Int32)
    target_ds.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        target_ds.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        target_ds.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(target_ds, [maskvalue], source_layer_ordered,options=["ATTRIBUTE="+attribute_name,"ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        target_ds.FlushCache()
        mask_arr = target_ds.GetRasterBand(1).ReadAsArray()
        return mask_arr

def select_in_shapefile(source_shapefile,out_Shape_Path,expression):
    """
    Select a part of the shapefile and make new shapefile with it
    ----------
    Parameters
    ----------
    source_shapefile:    string      Complete name of the source shapefile
    out_Shape_Path:      string      Complete name of the output shapefile
    expression:          string      SQL expression used for the selection
    -------
    Examples
    --------
    >>> import ogr,gdal
    >>> select_in_shapefile("Desserte.shp","Route.shp",'WHERE Importance=2')
    """
    #Recupere le driver
    driver = ogr.GetDriverByName('ESRI Shapefile')
    #Get information from source shapefile
    source_ds = ogr.Open(source_shapefile)
    source_layer = source_ds.GetLayer()    
    source_srs = source_layer.GetSpatialRef()
    source_type = source_layer.GetGeomType()
    try: source_layer_bis = source_ds.ExecuteSQL('SELECT * FROM '+str(source_layer.GetName())+' '+expression)   
    except: print("Erreur de syntaxe dans l'expression")
    # Initialize the output shapefile
    if os.path.exists(out_Shape_Path):driver.DeleteDataSource(out_Shape_Path)
    target_ds = driver.CreateDataSource(out_Shape_Path)
    layerName = os.path.splitext(os.path.split(out_Shape_Path)[1])[0]
    layer = target_ds.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('Route', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer_bis:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('Route',1)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Cleanup
    target_ds.Destroy()
    source_ds.Destroy()

def load_soil_param(Soil_param_file):
    """
    Load .csv file containing Rn and Rg according to each soil classes

    Parameters
    ----------
    Soil_param_file : .csv file with delimiter ";" and following cols
        SOIL_CLASSES;DESCRIPTION;RN_MIN;RN_MAX;RG_MIN_m;RG_MAX_m

    Returns
    -------
    Soil_Param : Numpy array with min/max value of range of Rn and Rg for
                 each soil classes

    """
    Soil_Param = np.loadtxt(Soil_param_file,delimiter=";",
                      skiprows=1,usecols=(2,3,4,5))
    newrow = [-1,-1,-1,-1]
    Soil_Param = np.float32(np.vstack([newrow,Soil_Param]))
    return Soil_Param


def select_and_convtoarray(shp_file,Extent,Csize,Issue_Rast,SQL_exp,fact=1):
    """
    Select entities from SQL expression and convert to array
    """
    #Get information from source shapefile
    source_ds = ogr.Open(shp_file)
    source_layer = source_ds.GetLayer()    
    source_srs = source_layer.GetSpatialRef()
    source_type = source_layer.GetGeomType()
          
    #Get raster dimension
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows = int((ymax-ymin)/float(Csize)+0.5)
    ncols = int((xmax-xmin)/float(Csize)+0.5)  
    
    #Select desired entities
    try: 
        SQLe = 'SELECT * FROM '+str(source_layer.GetName())+' '+SQL_exp
        source_layer_select = source_ds.ExecuteSQL(SQLe)   
    except: 
        print("Erreur de syntaxe dans l'expression")
        
    # Initialize temp shapefile
    select_shp = ogr.GetDriverByName("Memory").CreateDataSource("")
    layerName = "Onsenfout"
    layer = select_shp.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('IssueType', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer_select:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('IssueType',1)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Save in raster
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)         
    select_rast = gdal.GetDriverByName('MEM').Create('', int(ncols),
                                                     int(nrows), 1,
                                                     gdal.GDT_UInt16)
    select_rast.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        select_rast.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        select_rast.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(select_rast, [maskvalue], layer,
                              options=["ATTRIBUTE=IssueType","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        select_rast.FlushCache()
        mask_arr = select_rast.GetRasterBand(1).ReadAsArray()
    Issue_Rast = np.maximum(Issue_Rast,fact*mask_arr) 
    select_shp.Destroy()
    source_ds.Destroy()
    return Issue_Rast

def convtoarray(shp_file,Extent,Csize,Issue_Rast):
    """
    Convert shp to array
    """
    #Get information from source shapefile
    source_ds = ogr.Open(shp_file)
    source_layer = source_ds.GetLayer()    
    source_srs = source_layer.GetSpatialRef()
    source_type = source_layer.GetGeomType()

    #Get raster dimension
    xmin,xmax,ymin,ymax = Extent[0],Extent[1],Extent[2],Extent[3]
    nrows = int((ymax-ymin)/float(Csize)+0.5)
    ncols = int((xmax-xmin)/float(Csize)+0.5)  
        
    # Initialize temp shapefile
    select_shp = ogr.GetDriverByName("Memory").CreateDataSource("")
    layerName = "Onsenfout"
    layer = select_shp.CreateLayer(layerName, source_srs, source_type)
    layerDefinition = layer.GetLayerDefn()
    new_field = ogr.FieldDefn('IssueType', ogr.OFTInteger)
    layer.CreateField(new_field)
    ind=0
    for feat in source_layer:
        geometry = feat.GetGeometryRef()
        feature = ogr.Feature(layerDefinition)
        feature.SetGeometry(geometry)
        feature.SetFID(ind)
        feature.SetField('IssueType',1)
        # Save feature
        layer.CreateFeature(feature)
        # Cleanup
        feature.Destroy()
        ind +=1
    # Save in raster
    maskvalue = 1    
    xres=float(Csize)
    yres=float(Csize)
    geotransform=(xmin,xres,0,ymax,0, -yres)         
    select_rast = gdal.GetDriverByName('MEM').Create('', int(ncols), int(nrows),
                                                     1,gdal.GDT_UInt16)
    select_rast.SetGeoTransform(geotransform)
    if source_srs:
        # Make the target raster have the same projection as the source
        select_rast.SetProjection(source_srs.ExportToWkt())
    else:
        # Source has no projection (needs GDAL >= 1.7.0 to work)
        select_rast.SetProjection('LOCAL_CS["arbitrary"]')
    # Rasterize
    err = gdal.RasterizeLayer(select_rast, [maskvalue], layer,
                              options=["ATTRIBUTE=IssueType","ALL_TOUCHED=TRUE"])
    if err != 0:
        raise Exception("error rasterizing layer: %s" % err)
    else:
        select_rast.FlushCache()
        mask_arr = select_rast.GetRasterBand(1).ReadAsArray()
    Issue_Rast = np.maximum(Issue_Rast,mask_arr) 
    select_shp.Destroy()
    source_ds.Destroy()
    return Issue_Rast    

def BDTopo_to_np_array(Source,Extent,Csize,Road1,Road2,Road3,
                       Railway,Buildings,Water):
    """
    Use IGN BDTOPO to identify issues and water
    """
    ### Reseau routier   
    Base = Source+"TRANSPORT/TRONCON_DE_ROUTE.shp"
    #Route primaire sans tunnels 
    SQLprim =  "WHERE(POS_SOL > '-1' AND IMPORTANCE <= '3')"
    Road1 = select_and_convtoarray(Base,Extent,Csize,Road1,SQLprim)
    SQLprim =  "WHERE(POS_SOL > '-1' AND NATURE = 'Bretelle' "
    SQLprim += "AND IMPORTANCE <= '5')"
    Road1 = select_and_convtoarray(Base,Extent,Csize,Road1,SQLprim)
    # Route secondaire en supprimant les tunnels
    SQLsec =  "WHERE(POS_SOL > '-1' AND IMPORTANCE > '3' "
    SQLsec += "AND IMPORTANCE < '6' AND NATURE<> 'Bretelle' "
    SQLsec += "AND NATURE<> 'Chemin' AND NATURE<> 'Route empierrée')"
    Road2 = select_and_convtoarray(Base,Extent,Csize,Road2,SQLsec)
    # Chemin exploitation piste cyclable  en supprimant les tunnels
    SQLter =  "WHERE(POS_SOL > '-1'  AND IMPORTANCE >= '5' AND NATURE<> 'Chemin' "
    SQLter += "AND NATURE<> 'Bretelle' AND NATURE<> 'Route à 1 chaussée' "
    SQLter += "AND NATURE<> 'Route à 2 chaussées' "
    SQLter += "AND NATURE<> 'Sentier')"
    Road3 = select_and_convtoarray(Base,Extent,Csize,Road3,SQLter)

    ### Reseau ferre
    Base = Source+"TRANSPORT/TRONCON_DE_VOIE_FERREE.shp"          
    # Route primaire en supprimant les tunnels
    SQLtrain = "WHERE(POS_SOL > '-1' AND NATURE<> 'Sans objet' )"
    Railway = select_and_convtoarray(Base,Extent,Csize,Railway,SQLtrain) 
    
    ### Bati
    Base = Source+"TRANSPORT/PISTE_D_AERODROME.shp"      
    Buildings=convtoarray(Base,Extent,Csize,Buildings)
    Base = Source+"BATI/"
    ListeBati = [Base+"BATIMENT.shp",Base+"CIMETIERE.shp",
                 Base+"CONSTRUCTION_SURFACIQUE.shp",Base+"RESERVOIR.shp",
                 Base+"TERRAIN_DE_SPORT.shp"]
    for Bati in ListeBati:
        Buildings=convtoarray(Bati,Extent,Csize,Buildings)
        
    ### Eau  
    #Cours d'eau
    Base = Source+"HYDROGRAPHIE/TRONCON_HYDROGRAPHIQUE.shp"      
    SQLCint = "WHERE(POS_SOL = '0' AND PERSISTANC<> 'Permanent' "
    SQLCint +="AND PERSISTANC<> 'Inconnue'  )"
    Water = select_and_convtoarray(Base,Extent,Csize,Water,SQLCint)    
    SQLCperm = "WHERE(POS_SOL = '0' AND PERSISTANC='Permanent' )"
    Water = select_and_convtoarray(Base,Extent,Csize,Water,SQLCperm,2) 
    #Surface d'eau
    Base = Source+"HYDROGRAPHIE/SURFACE_HYDROGRAPHIQUE.shp"      
    SQLSint = "WHERE(POS_SOL = '0' AND PERSISTANC<> 'Permanent' "
    SQLSint +="AND PERSISTANC<> 'Inconnue'  AND NATURE<> 'Glacier, névé' )"
    Water = select_and_convtoarray(Base,Extent,Csize,Water,SQLSint)    
    SQLSperm = "WHERE(POS_SOL = '0' AND PERSISTANC='Permanent' "
    SQLSperm +="AND NATURE<> 'Glacier, névé' )"
    Water = select_and_convtoarray(Base,Extent,Csize,Water,SQLSperm,2) 
    
    return Road1,Road2,Road3,Railway,Buildings,Water

def BDForet_to_nparray(bdforet_shp,Extent,Csize,Foret):
    """
    Use IGN BDForet to identify forest
    """
    SQLFor = "WHERE(CODE_TFV <> 'LA4' AND CODE_TFV <> 'LA6' )"
    Foret = select_and_convtoarray(bdforet_shp,Extent,Csize,Foret,SQLFor)  
    return Foret

def conv_to_Soil_Classes(OSO,Forest,Railway,Buildings,Water,Dtm,Sl_arr):
    """
    Build Soil Classes raster for SylvaRock model from OSO
    """
    Soil = np.zeros_like(Forest)
    # Class 1 : Agricultural area
    tp = ((OSO>=5)*(OSO<16))>0
    Soil[tp]=1
    tp = ((OSO>=18)*(OSO<20)*(Dtm<=2000)*(Sl_arr<25))>0
    Soil[tp]=1
    tp = ((OSO<4)*(Sl_arr>=40))>0
    Soil[tp]=1  
    # Class 2 : Forest area
    tp = ((OSO>=16)*(OSO<18))>0
    Soil[tp]=2
    Soil[Forest==1]=2
    # Class 3 : Mineral area
    Soil[OSO>=20]=3  
    tp = ((OSO>=18)*(OSO<20)*(Dtm>2000)*(Sl_arr>=50))>0
    Soil[tp]=3
    # Class 4 : Urban area
    Soil[OSO==4]=4
    tp = ((OSO<4)*(Sl_arr<32))>0
    Soil[tp]=4    
    Soil[Railway==1]=4 
    Soil[Buildings==1]=4  
    # Class 5 : Permanent water area
    Soil[OSO==23]=5
    Soil[Water==2]=5
    # Class 6 : Temporary water area
    Soil[Water==1]=6
    # Class 7 : Screes area
    tp = ((OSO<4)*(Sl_arr<40)*(Sl_arr>=32))>0
    Soil[tp]=7  
    tp = ((OSO>=20)*(OSO<22)*(Sl_arr<40)*(Sl_arr>=32))>0
    Soil[tp]=7  
    # Class 8 : Meadows and grassland
    tp = ((OSO>=18)*(OSO<20)*(Dtm<=2000)*(Sl_arr>=25))>0
    Soil[tp]=8
    tp = ((OSO>=18)*(OSO<20)*(Dtm>2000)*(Sl_arr<50))>0
    Soil[tp]=8    
    return Soil
    
def resample_raster(raster_file,Extent,Csize,nodata_val=-9999,
                    save_file=False,out_file_name="",wkt_proj=None,
                    Oso_file=False,Dep_file=False):
    """
    Resample raster

    Parameters
    ----------
    raster_file : raster file to resample
    Extent : extent of the target area. Format [xmin,xmax,ymin,ymax]
    Csize : Cell size of the target area 
    nodata_val : nodataval of the target area. The default is -9999.
    save_file : save or not tiff file. The default is False.
    out_file_name : name of output file. The default is "".
    wkt_proj : projection associated. The default is None.

    Returns
    -------
    Array : np.array of input raster file on the target array

    """
    # Get info from source
    source_ds = gdal.Open(raster_file)    
    dataset_val = source_ds.GetRasterBand(1)
    src_nodata = dataset_val.GetNoDataValue()
    if src_nodata is None :
        minval,maxval,meanval,sdt = source_ds.GetRasterBand(1).GetStatistics(0, 1)
        src_nodata = np.nan
        
    Rxmin,Csize_x,a,Rymax,b,Csize_y = source_ds.GetGeoTransform()
    xmin,xmax,ymin,ymax  = Extent    
    
    if max(Csize_x,Csize_y)<Csize:
        algo = "average"
    elif max(Csize_x,Csize_y)==Csize:
        dec_x = 100*(abs(xmin-Rxmin)-abs(int(xmin-Rxmin)))/Csize
        dec_y = 100*(abs(ymax-Rymax)-abs(int(ymax-Rymax)))/Csize
        if dec_x>10 or dec_y >10:
            #decalage de plus de 10% entre cellule
            algo = 'bilinear'
        else:
            algo = 'near'
    else:
        algo = 'bilinear'
        
    if Oso_file:
        if Csize<=10:
            algo = "near"
        else:
            algo='mode'
    if Dep_file:
        algo= "average"
        newtype = gdal.GDT_Float32
        nbpixel = (Csize/Csize_x)**2
    else:
        newtype = dataset_val.DataType
    
    target_ds = gdal.Warp('',source_ds,format='VRT',
                          outputType=newtype,
                          outputBounds=(xmin,ymin,xmax,ymax), 
                          xRes=Csize,yRes=Csize,srcNodata=src_nodata,
                          dstNodata =nodata_val,resampleAlg=str(algo))
    
    dataset_val = target_ds.GetRasterBand(1)
    Array = dataset_val.ReadAsArray()
    Array[np.isnan(Array)]=nodata_val
    if source_ds.GetRasterBand(1).GetNoDataValue() is not None:
        Array[Array==src_nodata]=nodata_val
    else:
        Array[Array<minval]=nodata_val
    if Oso_file:
        Array[Array<=0]=nodata_val
        Array[Array>23]=nodata_val
    
    if Dep_file:
        Array = np.int16(Array*nbpixel+0.5)
    
    target_ds = None 
    source_ds = None   
    
    nrows,ncols=Array.shape    
    
    if np.min(Array)!=np.max(Array):        
        
        if dataset_val.DataType == gdal.GDT_Int32 :
            raster_type='INT32'
        elif dataset_val.DataType == gdal.GDT_Byte:
            raster_type='UINT8'
        elif dataset_val.DataType == gdal.GDT_UInt16:
            raster_type='UINT16'           
        elif dataset_val.DataType == gdal.GDT_Int16:
            raster_type='INT16'           
        elif  dataset_val.DataType == gdal.GDT_Float32:
            raster_type='FLOAT32'       
        elif dataset_val.DataType == gdal.GDT_Float16:
            raster_type='FLOAT16'
        
        if Dep_file:
            raster_type='INT16'  
        
        if save_file:
            ArrayToGtiff(Array,out_file_name,Extent,nrows,ncols,
                         Csize,wkt_proj,nodata_val,raster_type)
    
    return Array

def generate_HeadText(nrows,ncols,Extent,Csize,Nodata):  
    values = [ncols,nrows,Extent[0],Extent[2],Csize,Nodata]
    names = ['ncols', 'nrows', 'xllcorner', 'yllcorner', 'cellsize',
             'NODATA_value']  
    head_text=""
    for i,item in enumerate(names):
        head_text = head_text+item+(14-len(item))*" "+str(values[i])+"\n"
    return head_text

# Save integer like ASCII raster
def save_integer_ascii(file_name,nrows,ncols,Extent,Csize,Nodata,data):
    head_text=generate_HeadText(nrows,ncols,Extent,Csize,Nodata)
    np.savetxt(file_name, data, fmt='%i', delimiter=' ')
    with open(file_name, "r+") as f:
        old = f.read()
        f.seek(0)
        f.write(head_text + old)
        f.close()

# Save float like ASCII raster
def save_float_ascii(file_name,nrows,ncols,Extent,Csize,Nodata,data):
    head_text=generate_HeadText(nrows,ncols,Extent,Csize,Nodata)
    np.savetxt(file_name, data, fmt='%f', delimiter=' ')
    with open(file_name, "r+") as f:
        old = f.read()
        f.seek(0)
        f.write(head_text + old)
        f.close()

def identify_release_from_Lidar(Dtm_Lidar_file,Release_Lidar,nb_cell_dep,
                                Rs,Extent,Csize,wkt_proj,Area):
    Sl_arr = np.uint8(get_Slope(Dtm_Lidar_file,slope_format = 'degree')>=Release_Lidar)
    nrows,ncols = Sl_arr.shape
    names,values,src_proj,Extent_lid = raster_get_info(Dtm_Lidar_file)     
    ArrayToGtiff(Sl_arr,Rs+"dep_lidar",Extent_lid,nrows,ncols,
                 values[4],wkt_proj,255,'UINT8')   
    
    Dep =  resample_raster(Rs+"dep_lidar.tif",Extent,Csize,nodata_val=0,
                    save_file=False,out_file_name='',wkt_proj=wkt_proj,
                    Oso_file=False,Dep_file=True)
    
    Dep[Dep<nb_cell_dep]=0
    Dep[Area==0]=0    
    os.remove(Rs+"dep_lidar.tif")
    
    return Dep 
    
def prepa_area( Area_file,ResDir,Dtm_file,OSO_file,FFr_file,
                BDTopo_Source,BDForet_Source,Extent_to_stick_file,sylvarock_folder,
                Csize,release_method,Release_Lidar,nb_cell_dep,Dtm_Lidar_file,
                Release_Sl_h ,Release_Sl_b,Release_file,
                xmin = None,xmax = None,ymin = None,ymax = None):
    
    
    Rs=ResDir
    os.makedirs(Rs, exist_ok=True) 
        
    ### Get extent and Area
    Extent,nrows,ncols,Area,poly_area =get_target_area_extent(Area_file,Csize,
                                                              xmin,xmax,ymin,ymax,
                                                              Extent_to_stick_file,
                                                              buff=1000)
    
    
    BDTopo,BDForet  = get_targetDBTOPO_BDFORET(BDTopo_Source, BDForet_Source,
                                               BD_Departement, poly_area)
    
    wkt_proj,proj=get_proj_from_shp(FFr_file)
    
        
    ### Build issues
    Road1 = np.zeros((nrows,ncols),dtype=np.uint8)
    Road2 = np.copy(Road1)
    try:Road3 = np.uint8(shapefile_to_np_array(FFr_file,Extent,Csize,'rast'))
    except:Road3 = np.copy(Road1)
    Railway = np.copy(Road1)
    Buildings = np.copy(Road1)
    Water = np.copy(Road1)
    
    for Reg in BDTopo:
        Road1,Road2,Road3,Railway,Buildings,Water = BDTopo_to_np_array(Reg,
                                                                       Extent,Csize,
                                                                       Road1,Road2,
                                                                       Road3,Railway,
                                                                       Buildings,Water)
        Buildings[Buildings>0]=1
        Road1[Road1>0]=1
        Road2[Road2>0]=1
        Road3[Road3>0]=1
        Railway[Railway>0]=1   
    Issues=np.int16(Railway+10*Buildings+100*Road1+1000*Road2+10000*Road3)
    Issues[Issues==0]=-9999
    if np.unique(Issues.shape[0]==1):
        print("Issues pb")
    
    del Road1,Road2,Road3
    gc.collect()
    save_integer_ascii(Rs+"Issues.asc",nrows,ncols,Extent,Csize,-9999,Issues)
    del Issues
    gc.collect()

    ### Build Forest
    Forest = np.zeros((nrows,ncols),dtype=np.int16)
    
    for Dep in BDForet:
        Forest = BDForet_to_nparray(Dep,Extent,Csize,Forest)
        Forest[Forest>0]=1
    Forest[Forest ==0]=-9999
    if np.unique(Forest.shape[0]==1):
        print("Forest pb")
    save_integer_ascii(Rs+"Forest.asc",nrows,ncols,Extent,Csize,-9999,Forest)
    
    ### Resample OSO
    OSO = resample_raster(OSO_file,Extent,Csize,255,False,Rs+"OSO",wkt_proj,True)
    
    ### Resample Dtm
    Dtm = resample_raster(Dtm_file,Extent,Csize,-9999,True,Rs+"Dem",wkt_proj)
    save_float_ascii(Rs+"Dem.asc",nrows,ncols,Extent,Csize,-9999,Dtm)
    Sl_arr = get_Slope(Rs+"Dem.tif",slope_format = 'degree')
    os.remove(Rs+"Dem.tif")
    
    ### Generate Soil
    Soil = np.int16(conv_to_Soil_Classes(OSO,Forest,Railway,Buildings,Water,Dtm,Sl_arr))
    Soil[Soil==0]=-9999
    save_integer_ascii(Rs+"Soil_class.asc",nrows,ncols,Extent,Csize,-9999,Soil)
    del Soil,Dtm,Buildings,Railway
    gc.collect()
    
    ### Generate Releases area
    if release_method == 'lidar' :
        #'lidar' pour seuil nb cellule
        Releases = identify_release_from_Lidar(Dtm_Lidar_file,Release_Lidar,nb_cell_dep,
                                                Rs,Extent,Csize,wkt_proj,Area)
        
    elif release_method == 'shp' :                         
        #'shp' pour une couche shapefile
        Releases = Area*np.int16(shapefile_to_int8array(Release_file,Extent,Csize))
    elif release_method == 'pente' :
        #1seuil de pente
        Releases = auto_identify_release(Sl_arr,Area,Release_Sl_h)
    else:
        #2seuils de pente
        Releases = auto_identify_release2(Sl_arr,Area,Release_Sl_b,Release_Sl_h)
    Releases[Releases==0]=-9999
    save_integer_ascii(Rs+"Release_Area.asc",nrows,ncols,Extent,Csize,-9999,Releases)    
    
    
    ###copy main and soilparam to simu folder
    shutil.copyfile(sylvarock_folder+"main", Rs+"main")
    shutil.copyfile(sylvarock_folder+"Soil_Param.csv", Rs+"Soil_Param.csv")
    
    commandline = "chmod +x main"
    os.chdir(Rs)
    os.system(commandline)         
    
    print("All input files are created")
    
    

    
##############################################################################
### Create all necessary inputs for Sylvarock
##############################################################################

prepa_area(Area_file,ResDir,Dtm_file,OSO_file,FFr_file,
           BDTopo_Source,BDForet_Source,Extent_to_stick_file,sylvarock_folder,
           Csize,release_method,Release_Lidar,nb_cell_dep,Dtm_Lidar_file,
           Release_Sl_h ,Release_Sl_b,Release_file,
           xmin,xmax,ymin,ymax)